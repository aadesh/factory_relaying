/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * NIST-developed software is provided by NIST as a public
 * service. You may use, copy and distribute copies of the software in
 * any medium, provided that you keep intact this entire notice. You
 * may improve, modify and create derivative works of the software or
 * any portion of the software, and you may copy and distribute such
 * modifications or works. Modified works should carry a notice
 * stating that you changed the software and should note the date and
 * nature of any such change. Please explicitly acknowledge the
 * National Institute of Standards and Technology as the source of the
 * software.
 *
 * NIST-developed software is expressly provided "AS IS." NIST MAKES
 * NO WARRANTY OF ANY KIND, EXPRESS, IMPLIED, IN FACT OR ARISING BY
 * OPERATION OF LAW, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
 * NON-INFRINGEMENT AND DATA ACCURACY. NIST NEITHER REPRESENTS NOR
 * WARRANTS THAT THE OPERATION OF THE SOFTWARE WILL BE UNINTERRUPTED
 * OR ERROR-FREE, OR THAT ANY DEFECTS WILL BE CORRECTED. NIST DOES NOT
 * WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OF THE
 * SOFTWARE OR THE RESULTS THEREOF, INCLUDING BUT NOT LIMITED TO THE
 * CORRECTNESS, ACCURACY, RELIABILITY, OR USEFULNESS OF THE SOFTWARE.
 *
 * You are solely responsible for determining the appropriateness of
 * using and distributing the software and you assume all risks
 * associated with its use, including but not limited to the risks and
 * costs of program errors, compliance with applicable laws, damage to
 * or loss of data, programs or equipment, and the unavailability or
 * interruption of operation. This software is not intended to be used
 * in any situation where a failure could cause risk of injury or
 * damage to property. The software developed by NIST employees is not
 * subject to copyright protection within the United States.
 */


#include "ns3/lte-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store.h"
#include <cfloat>
#include <sstream>
#include <math.h>
#include "ns3/gnuplot.h"
#include "ns3/udp-echo-client.h"
#include <ns3/buildings-helper.h>
#include <ns3/buildings-module.h>
#include "ns3/flow-monitor-module.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/netanim-module.h"
#include <list>
#include <ctime>
#include <chrono>
#include <thread>
#include <unistd.h>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Factory_Realying");

uint32_t SERVICE_CODE = 33;
int TX_PACKETS = 0;

int RX_PACKETS = 0;

int TXem_PACKETS = 0;

int RXem_PACKETS = 0;
int active_relay = 0; 

// int TX_PACKETS1 = 0;

// int RX_PACKETS1 = 0; //Used to trace the number of Rx packets

std::list<int> relayNodes;
std::ofstream relayFile;
std::ofstream results;


/*Reason Code Values:  
  ns3::Ipv6L3Protocol::DROP_TTL_EXPIRED = 1, ns3::Ipv6L3Protocol::DROP_NO_ROUTE, ns3::Ipv6L3Protocol::DROP_INTERFACE_DOWN, ns3::Ipv6L3Protocol::DROP_ROUTE_ERROR,
  ns3::Ipv6L3Protocol::DROP_UNKNOWN_PROTOCOL, ns3::Ipv6L3Protocol::DROP_UNKNOWN_OPTION, ns3::Ipv6L3Protocol::DROP_MALFORMED_HEADER, ns3::Ipv6L3Protocol::DROP_FRAGMENT_TIMEOUT
*/
  void Ipv6L3ProtocolDrop(Ptr<OutputStreamWrapper> stream, std::string context,const Ipv6Header &header, Ptr<const Packet> packet, Ipv6L3Protocol::DropReason drop, Ptr<Ipv6> ipv6, uint32_t interface)
  {
    std::cout<< Simulator::Now ().GetNanoSeconds () / (double) 1e9 <<"\t"<<header<<"\t"<<packet->GetSize ()<<"\t"<<drop<<std::endl;

  }
/*
 * Trace sink function for logging when a packet is transmitted or received
 * at the application layer
 */
/**
 * Trace sink function for logging when a SD-RSRP measurement is done by a
 * Remote UE
 */
void
ReportUeSdRsrpMeasurementsTrace (Ptr<OutputStreamWrapper> stream,Ptr<NetDevice> ueDevice, uint16_t rnti, uint64_t relayId, uint32_t serviceCode, double sdRsrp)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);
  // std::cout<<"RRC: "<<rrc;

  // if (sdRsrp > -100)
  // {
  //   rrc->StopRelayService (SERVICE_CODE);
  // }

  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << rnti << "\t"
                        << relayId << "\t"
                        // << imsi << "\t"
                        << serviceCode << "\t"
                        << sdRsrp << "\t"
                        << std::endl;

}

static void
RxDrop (Ptr<const Packet> p)
{
  std::cout<<"RxDrop at " << Simulator::Now ().GetSeconds ()<<std::endl;
}

/**
 * Trace sink function for logging when a Remote UE selects a new Relay UE
 */
void
RelayUeSelectionTrace (Ptr<OutputStreamWrapper> stream, uint64_t imsi, uint32_t serviceCode, uint64_t currentRelayId, uint64_t selectedRelayId)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);

  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << imsi << "\t"
                        << serviceCode << "\t"
                        << currentRelayId << "\t"
                        << selectedRelayId << "\t"
                        << std::endl;
}

/**
 * Trace sink function for logging PC5 connection change of status
 */
void
Pc5ConnectionStatusTrace (Ptr<OutputStreamWrapper> stream, uint32_t selfUeId,  uint32_t peerUeId, LteSlUeRrc::RelayRole role,
                          LteSlBasicUeController::Pc5ConnectionStatus status)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);

  Ptr<MobilityModel> m1 = ns3::NodeList::GetNode(selfUeId)->GetObject<MobilityModel>();
  Ptr<MobilityModel> m2 = ns3::NodeList::GetNode(peerUeId)->GetObject<MobilityModel>(); 
  // std::cout<<"f1 output"<<m1<<std::endl; 

  Vector v1 = m1->GetPosition();
 
  Vector v2 = Vector (25, 25 , 7);

  Vector v3 = m2->GetPosition(); 

  Vector distance = v2-v1;

  Vector distance_relay = v3-v1;


  // std::cout<<"Position:"<< v1 << "\t"<<"Relay position:"<<v3<< "\t"<<"Distance"<<distance<<"\t"<<distance_relay<<std::endl;


  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << selfUeId << "\t"
                        << peerUeId << "\t"
                        << role << "\t"
                        << status << "\t"
                        << v1 <<"\t"
                        << v3 << "\t"
                        << distance <<"\t"
                        << distance_relay << "\t"
                        << std::endl;
}


void
UePacketTrace (Ptr<OutputStreamWrapper> stream, std::string context, Ptr<const Packet> p, const Address &srcAddrs, const Address &dstAddrs)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);

  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << context << "\t"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetPort () << "\t"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetPort () << "\t"
                        << p->GetSize () << "\t"
                        << std::endl;
  RX_PACKETS++;
}

void
UePacketInputTrace (Ptr<OutputStreamWrapper> stream, std::string context, Ptr<const Packet> p, const Address &srcAddrs, const Address &dstAddrs)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);

  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << context << "\t"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetPort () << "\t"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetPort () << "\t"
                        << p->GetSize () << "\t"
                        << std::endl;
  TX_PACKETS++;
}

void
UePacketTraceEm (Ptr<OutputStreamWrapper> stream, std::string context, Ptr<const Packet> p, const Address &srcAddrs, const Address &dstAddrs)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);

  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << context << "\t"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetPort () << "\t"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetPort () << "\t"
                        << p->GetSize () << "\t"
                        << std::endl;
  RXem_PACKETS++;
}

void
UePacketInputTraceEm (Ptr<OutputStreamWrapper> stream, std::string context, Ptr<const Packet> p, const Address &srcAddrs, const Address &dstAddrs)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);

  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << context << "\t"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (srcAddrs).GetPort () << "\t"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetIpv6 () << ":"
                        << Inet6SocketAddress::ConvertFrom (dstAddrs).GetPort () << "\t"
                        << p->GetSize () << "\t"
                        << std::endl;
  TXem_PACKETS++;
}


void NotifyConnectionEstablishedEnb (Ptr<OutputStreamWrapper> stream,std::string context, uint64_t imsi, uint16_t cellid, uint16_t rnti)
{
  std::cout << Simulator::Now ().GetSeconds () << " "
            << " eNB CellId " << cellid
            << ": successful connection of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;

       *stream->GetStream() << Simulator::Now().GetSeconds() <<"\t"<<cellid<<"\t"<<rnti<< "\t" << imsi << std::endl;
}
/*
 * Trace sink function to monitor primary cell RSRP and activate UE-to-Network
 * Relay service as Remote UE when the RSRP falls below a threshold
 */
void
RsrpMeasurementTrace (Ptr<OutputStreamWrapper> stream, const double threshold, Ptr<NetDevice> ueDevice, std::string context, uint64_t imsi, uint16_t cellId, double rsrp)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);
  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << imsi << "\t"
                        << cellId << "\t"
                        << rsrp << "\t"
                        << threshold << "\t"
                        << std::endl;

  Ptr<LteUeRrc> rrc = ueDevice->GetObject<LteUeNetDevice> ()->GetRrc ();
  PointerValue ptrOne;
  rrc->GetAttribute ("SidelinkConfiguration", ptrOne);
  Ptr<LteSlUeRrc> slUeRrc = ptrOne.Get<LteSlUeRrc> ();

  //Avoid that in-coverage Remote UE starts Relay service before cell
  //attachment (happening at the first RSRP report: 200 ms)

  if (Simulator::Now ().GetMilliSeconds () < 400)
    {
      return;
    }
  else
    {
      //If the RSRP of the primary cell is below the threshold and
      //the UE is not in Remote UE role already (i.e., already monitoring Discovery Relay Announcements (we are using discovery Model A))
      //then start UE-to-Network Relay Service as Remote UE
      if (rsrp < threshold && !slUeRrc->IsMonitoringRelayServiceCode (LteSlDiscHeader::DISC_RELAY_ANNOUNCEMENT, SERVICE_CODE))
        {
          rrc->StartRelayService (SERVICE_CODE, LteSlUeRrc::ModelA, LteSlUeRrc::RemoteUE);
          active_relay++;
        }
        //For switching 
        if (rsrp > threshold)
        {
          rrc->StopRelayService (SERVICE_CODE);
          // active_relay--;
        }
    }
}

/*
 * Trace sink function to monitor primary cell RSRP and activate UE-to-Network
 * Relay service as Remote UE when the RSRP falls below a threshold
 */
void
AllRsrpMeasurementTrace (Ptr<OutputStreamWrapper> stream, std::string context, uint64_t imsi, uint16_t cellId, double rsrp)
{
  std::ostringstream oss;
  stream->GetStream ()->precision (6);



  Ptr<MobilityModel> m1 = ns3::NodeList::GetNode(imsi)->GetObject<MobilityModel>();
  if (m1 != 0)
  {
  // std::cout<<"position"<<m1;
  
  // Ptr<MobilityModel> m2 = ns3::NodeList::GetNode(peerUeId)->GetObject<MobilityModel>();  

  Vector v1 = m1->GetPosition();
  // std::cout<<"vector"<<v1;
 
  Vector v2 = Vector (25, 25 , 7);

  // Vector v3 = m2->GetPosition(); 

  Vector distance = v2-v1;

  // Vector distance_relay = v3-v1;

  stream->GetStream ()->precision (6);
  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << imsi << "\t"
                        << cellId << "\t"
                        << rsrp << "\t"
                        // << threshold << "\t"
                        << v1 <<"\t"
                        << distance<<"\t"
                        << std::endl;
}


}

/*
 * Trace sink function to monitor primary cell RSRP and activate UE-to-Network
 * Relay service as Remote UE when the RSRP falls below a threshold
 */
void
RsrpMeasurementTrace_relayUe (Ptr<OutputStreamWrapper> stream, const double threshold, Ptr<NetDevice> ueDevice, std::string context, uint64_t imsi, uint16_t cellId, double rsrp)
{
  std::ostringstream oss;
  
  stream->GetStream ()->precision (6);
  *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
                        << imsi << "\t"
                        << cellId << "\t"
                        << rsrp << "\t"
                        << threshold << "\t"
                        << std::endl;

  Ptr<LteUeRrc> rrc = ueDevice->GetObject<LteUeNetDevice> ()->GetRrc ();
  PointerValue ptrOne;
  rrc->GetAttribute ("SidelinkConfiguration", ptrOne);
  Ptr<LteSlUeRrc> slUeRrc = ptrOne.Get<LteSlUeRrc> ();

  bool found = (std::find(relayNodes.begin(), relayNodes.end(), imsi) != relayNodes.end());

  relayFile.open ("Relaying.txt", std::ofstream::out | std::ofstream::app);
  
  //Avoid that in-coverage Remote UE starts Relay service before cell
  //attachment (happening at the first RSRP report: 200 ms)

  if (Simulator::Now ().GetMilliSeconds () < 400)
    {
      return;
    }
  else
    {
      //If the RSRP of the primary cell is below the threshold and
      //the UE is not in Remote UE role already (i.e., already monitoring Discovery Relay Announcements (we are using discovery Model A))
      //then start UE-to-Network Relay Service as Remote UE
      if (rsrp > threshold && !found)
        {
          rrc->StartRelayService (SERVICE_CODE, LteSlUeRrc::ModelA, LteSlUeRrc::RelayUE);
          relayFile<<imsi<<"\t"<<"Started Relaying Service at"<<"\t"<<Simulator::Now ().GetNanoSeconds () / (double) 1e9<<std::endl;
          std::cout<<"Relay value2:"<<found;
          std::cout<< "Start relay service:"<<imsi <<std::endl; 
          relayNodes.push_back(imsi);
        }
      else if (rsrp < threshold && found)
        {
          rrc->StopRelayService (SERVICE_CODE);
          relayFile<<imsi<<"\t"<<"Stopped Relaying Service at"<<"\t"<<Simulator::Now ().GetNanoSeconds () / (double) 1e9<<std::endl;
          std::cout<<"stop service for:"<<imsi<<std::endl;
          std::cout<<"Relay value3:"<<found;
          relayNodes.remove(imsi);
        }
    }
    relayFile.close();
}


/*
 * Trace sink function for logging when PC5 signaling messages are received
 */
void
TraceSinkPC5SignalingPacketTrace (Ptr<OutputStreamWrapper> stream, uint32_t srcL2Id, uint32_t dstL2Id, Ptr<Packet> p)
{
  LteSlPc5SignallingMessageType lpc5smt;
  p->PeekHeader (lpc5smt);
  *stream->GetStream () << Simulator::Now ().GetSeconds () << "\t" << srcL2Id << "\t" << dstL2Id << "\t" << lpc5smt.GetMessageName () << std::endl;
}


int main (int argc, char *argv[])
{

  double simTime = 90.0; //s //Simulation time (in seconds) updated automatically based on number of nodes

  uint32_t nRelayUes = 80 ;
  uint32_t nRemoteUes = 20;



  
  double threshold = -70.00; //dBm
  double threshold_relay = -75.00;


  CommandLine cmd;

  // cmd.AddValue ("useRelay", "The Remote UEs will use Relay Service", useRelay);
  cmd.AddValue ("threshold", "The RSRP value (dBm) for which the Remote UE will start Relay Service", threshold);

  cmd.Parse (argc, argv);
 
  double startTimeRelay [nRelayUes];
  double startTimeRemote [nRemoteUes];
 
  double timeBetweenRemoteStarts = 4 ; //s
  
  double timeBetweenRelayStarts = nRemoteUes*0.04 + timeBetweenRemoteStarts; //s

  for (uint32_t ryIdx = 0; ryIdx < nRelayUes; ryIdx++)
    {
      startTimeRelay [ryIdx] =  (timeBetweenRelayStarts * ryIdx);

      NS_LOG_INFO ("Relay UE Idx " << ryIdx << " start time " << startTimeRelay [ryIdx] << "s");

      for (uint32_t rm = 0; rm < nRemoteUes; ++rm)
        {
          uint32_t rmIdx = ryIdx * nRemoteUes + rm;
          startTimeRemote [rmIdx] = startTimeRelay [ryIdx] + timeBetweenRemoteStarts * (rm + 1);
          NS_LOG_INFO ("Remote UE Idx " << rmIdx << " start time " << startTimeRemote [rmIdx] << "s");
        }
    }

  // // Calculate simTime based on relay service starts and give 10 s of traffic for the last one
  // simTime = startTimeRemote [(nRelayUes - 1)] + 3.0 + 50.0; //s
  // // Calculate simTime to give 10 s of traffic for the Remote UE
  // simTime = startTimeRemoteApp + 30.0; //s

  NS_LOG_INFO ("Configuring default parameters...");

  //Configure the UE for UE_SELECTED scenario
  Config::SetDefault ("ns3::LteUeMac::SlGrantMcs", UintegerValue (16));
  Config::SetDefault ("ns3::LteUeMac::SlGrantSize", UintegerValue (6)); //The number of RBs allocated per UE for Sidelink
  Config::SetDefault ("ns3::LteUeMac::Ktrp", UintegerValue (1));
  Config::SetDefault ("ns3::LteUeMac::UseSetTrp", BooleanValue (false));
  Config::SetDefault ("ns3::LteUeMac::SlScheduler", StringValue ("MaxCoverage")); //Values include Fixed, Random, MinPrb, MaxCoverage

    //Set the frequency
  Config::SetDefault ("ns3::LteEnbNetDevice::DlEarfcn", UintegerValue (6250));
  Config::SetDefault ("ns3::LteUeNetDevice::DlEarfcn", UintegerValue (6250));
  Config::SetDefault ("ns3::LteEnbNetDevice::UlEarfcn", UintegerValue (24250));
  Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue (100));
  Config::SetDefault ("ns3::LteEnbNetDevice::UlBandwidth", UintegerValue (100));


  //Reduce frequency of CQI report to allow for sidelink transmissions
  Config::SetDefault ("ns3::LteUePhy::DownlinkCqiPeriodicity", TimeValue (MilliSeconds (500)));


  //error Model
  Config::SetDefault ("ns3::LteSpectrumPhy::SlCtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::SlDataErrorModelEnabled", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlFullDuplexEnabled", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::DropRbOnCollisionEnabled", BooleanValue (false));
  // Config::SetDefault ("ns3::LteSpectrumPhy::SlDiscoveryErrorModelEnabled", BooleanValue (false));



  Config::SetDefault ("ns3::LteUePhy::TxPower", DoubleValue(20)); //0.1 W =20
  Config::SetDefault ("ns3::LteUePhy::NoiseFigure", DoubleValue(9));
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue(26)); // 0.5 W =26
  Config::SetDefault ("ns3::LteEnbPhy::NoiseFigure", DoubleValue(5)); 
  //Create the lte helper
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  //Create and set the EPC helper
  Ptr<PointToPointEpcHelper>  epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);




  //Create Sidelink helper and set lteHelper
  Ptr<LteSidelinkHelper> proseHelper = CreateObject<LteSidelinkHelper> ();
  proseHelper->SetLteHelper (lteHelper);

  //Connect Sidelink controller and Sidelink Helper
  Config::SetDefault ("ns3::LteSlBasicUeController::ProseHelper", PointerValue (proseHelper));

  //Configure Relay UE (re)selection algorithm (RandomNoReselection|MaxSDRsrpNoReselection|MaxSDRsrp | ModifiedSdRsrp)
  std::string relaySelection ( "ModifiedSdRsrp");
  Config::SetDefault ("ns3::LteSlBasicUeController::RelayUeSelectionAlgorithm", StringValue (relaySelection)); 
  // lteHelper->SetSchedulerType ("ns3::RrSlFfMacScheduler");



  //Set pathloss model
  // lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::Hybrid3gppPropagationLossModel"));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::HybridBuildingsPropagationLossModel"));
  lteHelper->SetPathlossModelAttribute ("ShadowSigmaExtWalls", DoubleValue (4));
  lteHelper->SetPathlossModelAttribute ("ShadowSigmaOutdoor", DoubleValue (1));
  lteHelper->SetPathlossModelAttribute ("ShadowSigmaIndoor", DoubleValue (1.5));
  lteHelper->SetSpectrumChannelType ("ns3::MultiModelSpectrumChannel");
  lteHelper->SetPathlossModelAttribute ("Frequency", DoubleValue (800e6));




  //Enable Sidelink
  lteHelper->SetAttribute ("UseSidelink", BooleanValue (true));

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create the Internet
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);

  //Create nodes (eNb + UEs)
  NodeContainer enbNode;
  enbNode.Create (1);

  NodeContainer relayUeNodes;
  relayUeNodes.Create (nRelayUes);

  NodeContainer remoteUeNodes;
  remoteUeNodes.Create (nRemoteUes);

  // NodeContainer allUeNodes = NodeContainer (relayUeNodes,remoteUeNodes);
  NodeContainer allUeNodes;
  allUeNodes.Add(remoteUeNodes);
  allUeNodes.Add(relayUeNodes);



   //obstacle 1 
  Ptr<Building> building2 = CreateObject<Building> ();
  building2->SetBoundaries (Box (0, 10, 0, 10, 0.0, 15.0));
  building2->SetBuildingType (Building::Commercial);
  building2->SetExtWallsType (Building::ConcreteWithoutWindows);

    //obstacle 2
  Ptr<Building> building3 = CreateObject<Building> ();
  building3->SetBoundaries (Box (10, 14, 10,15, 0.0, 15.0));
  building3->SetBuildingType (Building::Commercial);
  building3->SetExtWallsType (Building::ConcreteWithoutWindows);

    //obstacle 3 
  Ptr<Building> building4 = CreateObject<Building> ();
  building4->SetBoundaries (Box (35, 30, 0,5, 0.0, 15.0));
  building4->SetBuildingType (Building::Commercial);
  building4->SetExtWallsType (Building::ConcreteWithoutWindows);



      //obstacle 5 
  Ptr<Building> building6 = CreateObject<Building> ();
  building6->SetBoundaries (Box (45, 50, 20, 30, 0.0, 15.0));
  building6->SetBuildingType (Building::Commercial);
  building6->SetExtWallsType (Building::ConcreteWithoutWindows);

    //obstacle 6
  Ptr<Building> building7 = CreateObject<Building> ();
  building7->SetBoundaries (Box (35, 30, 25,30, 0.0, 15.0));
  building7->SetBuildingType (Building::Commercial);
  building7->SetExtWallsType (Building::ConcreteWithoutWindows);


    //obstacle 7
  Ptr<Building> building8 = CreateObject<Building> ();
  building8->SetBoundaries (Box (25, 15, 25,15, 0.0, 15.0));
  building8->SetBuildingType (Building::Commercial);
  building8->SetExtWallsType (Building::ConcreteWithoutWindows);


    //obstacle 8
  Ptr<Building> building9 = CreateObject<Building> ();
  building9->SetBoundaries (Box (35, 32, 35,30, 0.0, 15.0));
  building9->SetBuildingType (Building::Commercial);
  building9->SetExtWallsType (Building::ConcreteWithoutWindows);

      //obstacle 9
  Ptr<Building> building10 = CreateObject<Building> ();
  building10->SetBoundaries (Box (35, 32, 45,50, 0.0, 15.0));
  building10->SetBuildingType (Building::Commercial);
  building10->SetExtWallsType (Building::ConcreteWithoutWindows);

        //obstacle 10
  Ptr<Building> building11 = CreateObject<Building> ();
  building10->SetBoundaries (Box (0, 10, 45,50, 0.0, 15.0));
  building10->SetBuildingType (Building::Commercial);
  building10->SetExtWallsType (Building::ConcreteWithoutWindows);

          //obstacle 11
  Ptr<Building> building12 = CreateObject<Building> ();
  building10->SetBoundaries (Box (0, 10, 25,35, 0.0, 15.0));
  building10->SetBuildingType (Building::Commercial);
  building10->SetExtWallsType (Building::ConcreteWithoutWindows);

            //obstacle 12
  Ptr<Building> building13 = CreateObject<Building> ();
  building10->SetBoundaries (Box (15,25 , 35,45, 0.0, 15.0));
  building10->SetBuildingType (Building::Commercial);
  building10->SetExtWallsType (Building::ConcreteWithoutWindows);
  

  



  //Position of the nodes
  //eNodeB
  Ptr<ListPositionAllocator> positionAllocEnb = CreateObject<ListPositionAllocator> ();
  positionAllocEnb->Add (Vector (25,25,6));
  MobilityHelper mobilityeNodeB;
  mobilityeNodeB.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityeNodeB.SetPositionAllocator (positionAllocEnb);
  mobilityeNodeB.Install (enbNode);
  BuildingsHelper::Install (enbNode);

  // Static nodes
  Ptr<ListPositionAllocator> posAllocUeStatic = CreateObject<ListPositionAllocator> ();

   for (uint8_t j = 0; j < 8; j++)
     {
       for (uint8_t i = 0; i < 10; i++)
         {
           posAllocUeStatic->Add(Vector( i*5, j*6, 1));
         }
     }
   MobilityHelper mobilityHelperUeStatic;
   mobilityHelperUeStatic.SetMobilityModel ( "ns3::RandomWaypointMobilityModel");
    mobilityHelperUeStatic.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                            "Mode", StringValue ("Time"),
                            "Time", StringValue ("1s"),
                            "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=0.1]"),
                            "Bounds", StringValue ("0|50|0|50"));

   mobilityHelperUeStatic.SetPositionAllocator (posAllocUeStatic);
   mobilityHelperUeStatic.Install (relayUeNodes);
   BuildingsHelper::Install (relayUeNodes); 
  int32_t runValue = 33; // 2 7 15 12 17 24 27 53 79 99 81 63/66 || 65 35 71 33 34 56 22 42
  RngSeedManager::SetSeed (2);
  RngSeedManager::SetRun (runValue);
   //Mobile Node
  Ptr<RandomBoxPositionAllocator> randomUePositionAlloc = CreateObject<RandomBoxPositionAllocator> ();
  Ptr<UniformRandomVariable> xVal = CreateObject<UniformRandomVariable> ();
  xVal->SetAttribute ("Min", DoubleValue (0))
;  xVal->SetAttribute ("Max", DoubleValue (50));
  randomUePositionAlloc->SetAttribute ("X", PointerValue (xVal));
  Ptr<UniformRandomVariable> yVal = CreateObject<UniformRandomVariable> ();
  yVal->SetAttribute ("Min", DoubleValue (0));
  yVal->SetAttribute ("Max", DoubleValue (50));
  randomUePositionAlloc->SetAttribute ("Y", PointerValue (yVal));
  Ptr<ConstantRandomVariable> ueRandomVarZ = CreateObject<ConstantRandomVariable>();
  ueRandomVarZ->SetAttribute("Constant", DoubleValue(0.3));
 
  // zVal->SetAttribute ("Min", DoubleValue (macroUeBox.zMin));
  // zVal->SetAttribute ("Max", DoubleValue (macroUeBox.zMax));
  randomUePositionAlloc->SetZ(ueRandomVarZ);
  MobilityHelper mobilityHelperUeMobile;
   // mobilityHelperUeMobile.SetMobilityModel( "ns3::RandomWaypointMobilityModel");
    mobilityHelperUeMobile.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                            "Mode", StringValue ("Time"),
                            "Time", StringValue ("1s"),
                            "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=3.0]"),
                            "Bounds", StringValue ("0|50|0|50"));
  
  mobilityHelperUeMobile.SetPositionAllocator (randomUePositionAlloc);
  mobilityHelperUeMobile.Install (remoteUeNodes);
  BuildingsHelper::Install (remoteUeNodes);


  AsciiTraceHelper ascii;

  //Install LTE devices to the nodes
  NetDeviceContainer enbDevs = lteHelper->InstallEnbDevice (enbNode);
  NetDeviceContainer relayUeDevs = lteHelper->InstallUeDevice (relayUeNodes);
  NetDeviceContainer remoteUeDevs = lteHelper->InstallUeDevice (remoteUeNodes);
  NetDeviceContainer allUeDevs = NetDeviceContainer (relayUeDevs, remoteUeDevs);


  //Configure eNodeB for Sidelink
  Ptr<LteSlEnbRrc> enbSidelinkConfiguration = CreateObject<LteSlEnbRrc> ();
  enbSidelinkConfiguration->SetSlEnabled (true);

  //-Configure Sidelink communication pool
  // enbSidelinkConfiguration->SetDefaultPool (proseHelper->GetDefaultSlCommTxResourcesSetupUeSelected ());

    //Configure communication pool
  LteRrcSap::SlCommTxResourcesSetup pool;

  pool.setup = LteRrcSap::SlCommTxResourcesSetup::UE_SELECTED;
  pool.ueSelected.havePoolToRelease = false;
  pool.ueSelected.havePoolToAdd = true;
  pool.ueSelected.poolToAddModList.nbPools = 1;
  pool.ueSelected.poolToAddModList.pools[0].poolIdentity = 1;

  LteSlResourcePoolFactory pfactory;
  //Control
  pfactory.SetControlPeriod ("sf12"); //12 subframe for 1 period
  pfactory.SetControlBitmap (0x00000F); //4 subframes for PSCCH 
  pfactory.SetControlOffset (0);
  pfactory.SetControlPrbNum (50);
  pfactory.SetControlPrbStart (0);
  pfactory.SetControlPrbEnd (99);
  //Data
  pfactory.SetDataBitmap (0x00000FFF); //FFFFFFFFFFFFFF
  pfactory.SetDataOffset (4); //After 4 subframes of PSCCH
  pfactory.SetDataPrbNum (50);
  pfactory.SetDataPrbStart (0);
  pfactory.SetDataPrbEnd (99);

  pool.ueSelected.poolToAddModList.pools[0].pool =  pfactory.CreatePool ();
    //Add the pool as a default pool
  enbSidelinkConfiguration->SetDefaultPool (pool);

  //-Enable Sidelink discovery
  enbSidelinkConfiguration->SetDiscEnabled (true);

  //-Configure Sidelink discovery pool
  // enbSidelinkConfiguration->AddDiscPool (proseHelper->GetDefaultSlDiscTxResourcesSetupUeSelected ());
  LteRrcSap::SlDiscTxResourcesSetup discPool;
  discPool.setup =  LteRrcSap::SlDiscTxResourcesSetup::UE_SELECTED;
  discPool.ueSelected.havePoolToRelease = false;
  discPool.ueSelected.havePoolToAdd = true;
  discPool.ueSelected.poolToAddModList.nbPools = 1;
  discPool.ueSelected.poolToAddModList.pools[0].poolIdentity = 1;

  LteSlDiscResourcePoolFactory pDiscFactory;
  pDiscFactory.SetDiscCpLen ("NORMAL");
  pDiscFactory.SetDiscPeriod ("rf32");
  pDiscFactory.SetNumRetx (2);
  pDiscFactory.SetNumRepetition (1);
  pDiscFactory.SetDiscPrbNum (10);
  pDiscFactory.SetDiscPrbStart (10);
  pDiscFactory.SetDiscPrbEnd (99);
  pDiscFactory.SetDiscOffset (0);
  pDiscFactory.SetDiscBitmap (0x11111);
  pDiscFactory.SetDiscTxProbability ("p100");

  discPool.ueSelected.poolToAddModList.pools[0].pool =  pDiscFactory.CreatePool ();

  enbSidelinkConfiguration->AddDiscPool (discPool);


  //-Configure UE-to-Network Relay parameters
  // enbSidelinkConfiguration->SetDiscConfigRelay (proseHelper->GetDefaultSib19DiscConfigRelay ());
  LteRrcSap::Sib19DiscConfigRelay discConfigRelay;
  discConfigRelay.haveRemoteUeConfig = true;
  discConfigRelay.remoteUeConfig.haveReselectionInfoIc = true;
  discConfigRelay.remoteUeConfig.reselectionInfoIc.filterCoefficient = 0;
  discConfigRelay.remoteUeConfig.reselectionInfoIc.minHyst = 0;
  discConfigRelay.remoteUeConfig.reselectionInfoIc.qRxLevMin = -70;
  enbSidelinkConfiguration->SetDiscConfigRelay (discConfigRelay);

  //Install eNodeB Sidelink configuration on the eNodeB devices
  lteHelper->InstallSidelinkConfiguration (enbDevs, enbSidelinkConfiguration);

  //Preconfigure UEs for Sidelink
  Ptr<LteSlUeRrc> ueSidelinkConfiguration = CreateObject<LteSlUeRrc> ();
  ueSidelinkConfiguration->SetSlEnabled (true);
  uint8_t nb = 3;
  ueSidelinkConfiguration->SetDiscTxResources (nb);
  //-Configure Sidelink preconfiguration
  // Relay UEs: Empty configuration
  LteRrcSap::SlPreconfiguration preconfigurationRelay;
  
  // preconfigurationRelay.preconfigGeneral.carrierFreq = 18100;
  // preconfigurationRelay.preconfigGeneral.slBandwidth = 100;


  //-Enable Sidelink discovery
  ueSidelinkConfiguration->SetDiscEnabled (true);
  //-Set frequency for Sidelink discovery messages monitoring
  ueSidelinkConfiguration->SetDiscInterFreq (enbDevs.Get (0)->GetObject<LteEnbNetDevice> ()->GetUlEarfcn ());
  // ueSidelinkConfiguration->SetDiscInterFreq ();

  //Install UE Sidelink configuration on the UE devices with the corresponding preconfiguration
  ueSidelinkConfiguration->SetSlPreconfiguration (preconfigurationRelay);
  lteHelper->InstallSidelinkConfiguration (allUeDevs, ueSidelinkConfiguration);



  //Install the IP stack on the UEs and assign network IP addresses
  internet.Install (relayUeNodes);
  internet.Install (remoteUeNodes);
  Ipv6InterfaceContainer ueIpIfaceRelays;
  Ipv6InterfaceContainer ueIpIfaceRemotes;
  ueIpIfaceRelays = epcHelper->AssignUeIpv6Address (relayUeDevs);
  ueIpIfaceRemotes = epcHelper->AssignUeIpv6Address (remoteUeDevs);

  //Set the default gateway for the UEs
  Ipv6StaticRoutingHelper Ipv6RoutingHelper;
  for (uint32_t u = 0; u < allUeNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = allUeNodes.Get (u);
      Ptr<Ipv6StaticRouting> ueStaticRouting = Ipv6RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv6> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress6 (), 1);
    }

  //Configure IP for the nodes in the Internet (PGW and RemoteHost)
  Ipv6AddressHelper ipv6h;
  ipv6h.SetBase (Ipv6Address ("6001:db80::"), Ipv6Prefix (64));
  Ipv6InterfaceContainer internetIpIfaces = ipv6h.Assign (internetDevices);
  internetIpIfaces.SetForwarding (0, true);
  internetIpIfaces.SetDefaultRouteInAllNodes (0);

  //Set route for the Remote Host to join the LTE network nodes
  Ipv6StaticRoutingHelper ipv6RoutingHelper;
  Ptr<Ipv6StaticRouting> remoteHostStaticRouting = ipv6RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv6> ());
  remoteHostStaticRouting->AddNetworkRouteTo ("7777:f000::", Ipv6Prefix (60), internetIpIfaces.GetAddress (0, 1), 1, 0);

  //Configure UE-to-Network Relay network
  proseHelper->SetIpv6BaseForRelayCommunication ("7777:f00e::", Ipv6Prefix (48));

  //Configure route between PGW and UE-to-Network Relay network
  proseHelper->ConfigurePgwToUeToNetworkRelayRoute (pgw);

  //Attach UEs to the eNB
  lteHelper->Attach (relayUeDevs);
  lteHelper->Attach (remoteUeDevs);

   Ipv6Address ip;

  Ptr<OutputStreamWrapper> MobiStream = ascii.CreateFileStream ("UeMobilityTrace_static.txt");
  for (uint32_t u = 0; u < allUeNodes.GetN (); ++u)
    {
      ip =  allUeNodes.Get(u)->GetObject<Ipv6>()->GetAddress(1,1).GetAddress();

      Ptr<MobilityModel> m1 = allUeNodes.Get(u)->GetObject<MobilityModel>();
      std::cout<<"IP Address: "<<ip<<"IMSI: "
      << allUeNodes.Get(u)->GetDevice(0)->GetObject <LteUeNetDevice>()->GetImsi() << "POS:"<< m1->GetPosition() <<std::endl;
      // std::cout<<"Power"<< ueNodes.Get(u)-> GetDevice(0)->GetObject<LteUePhy>()-> GetTxPower()<<std::endl;
       *MobiStream->GetStream () << Simulator::Now ().GetSeconds () << "\t" << allUeNodes.Get(u)->GetDevice(0)->GetObject <LteUeNetDevice>()->GetImsi() << "\t"<<m1->GetPosition()<<"\t" <<ip << std::endl;
    } 


  double power =0.0;
  Ptr<LteUePhy> uePhy =  remoteUeDevs.Get (0)->GetObject<LteUeNetDevice> ()->GetPhy (); // 0 because there is only one Component Carrier when carrier aggregation is not used
  Simulator::Schedule(Seconds(10),&LteUePhy::SetTxPower, uePhy, power);

  ///*** Configure applications ***///
  //For each Remote UE, we have a pair (UpdEchoClient, UdpEchoServer)
  //Each Remote UE has an assigned port
  //UdpEchoClient installed in the Remote UE, sending to the echoServerAddr
  //in the corresponding Remote UE port
  //UdpEchoServer installed in the echoServerNode, listening to the
  //corresponding Remote UE port
  Ptr<ExponentialRandomVariable> emergencyIntervalSeconds = CreateObject<ExponentialRandomVariable> ();
  emergencyIntervalSeconds->SetAttribute("Mean", DoubleValue(simTime));     // To be defined
  emergencyIntervalSeconds->SetAttribute("Bound", DoubleValue(simTime));

  Ipv6Address echoServerAddr = internetIpIfaces.GetAddress (1, 1);
  uint16_t echoPortBase = 50000;

  ApplicationContainer serverApps;
  ApplicationContainer clientApps;

  uint16_t remUePort = echoPortBase + 1;
  uint16_t otherPort = 1234;


  
  ApplicationContainer singleServerApp;
  ApplicationContainer singleClientApp;

  ApplicationContainer emServerApp;
  ApplicationContainer emClientApp;

  std::string echoServerNode ("RemoteHost");
  std::ostringstream oss;
  Ptr<OutputStreamWrapper> packetOutputStream = ascii.CreateFileStream ("AppPacketTrace_remoteUe.txt");
  *packetOutputStream->GetStream () << "time(sec)\ttx/rx\tC/S\tIMSI\tIP[src]\tIP[dst]\tPktSize(bytes)" << std::endl;

    Ptr<OutputStreamWrapper> packetInputStream = ascii.CreateFileStream ("AppPacketInputTrace_remoteUe.txt");
  *packetInputStream->GetStream () << "time(sec)\ttx/rx\tC/S\tIMSI\tIP[src]\tIP[dst]\tPktSize(bytes)" << std::endl;

    Ptr<OutputStreamWrapper> packetOutputStreamemTest = ascii.CreateFileStream ("AppPacketTrace_remoteUeemTest.txt");
  *packetOutputStream->GetStream () << "time(sec)\ttx/rx\tC/S\tIMSI\tIP[src]\tIP[dst]\tPktSize(bytes)" << std::endl;

    Ptr<OutputStreamWrapper> packetInputStreamemTest = ascii.CreateFileStream ("AppPacketInputTrace_remoteUeemTest.txt");
  *packetInputStream->GetStream () << "time(sec)\ttx/rx\tC/S\tIMSI\tIP[src]\tIP[dst]\tPktSize(bytes)" << std::endl;

  Address address= Address(internetIpIfaces.GetAddress(1,1));

  for (uint32_t u=0; u< remoteUeNodes.GetN(); ++u)
  {

  remUePort++;
  otherPort++;
  UdpEchoServerHelper echoServerHelper (remUePort);
  singleServerApp.Add (echoServerHelper.Install (remoteHost));
  serverApps.Add (singleServerApp);
   // UdpServerHelper EmServer (otherPort);
  UdpEchoServerHelper EmServer (otherPort);
  // PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory",Inet6SocketAddress (Ipv6Address::GetAny (), otherPort));
  emServerApp.Add(EmServer.Install (remoteHost));
  serverApps.Add (emServerApp);

   singleServerApp.Start (Seconds (1));
  singleServerApp.Stop (Seconds (simTime));

   emServerApp.Start (Seconds (1));
  emServerApp.Stop (Seconds (simTime));


  //UdpEchoClient in the Remote UE
  UdpEchoClientHelper echoClientHelper (echoServerAddr);
  echoClientHelper.SetAttribute ("MaxPackets", UintegerValue (100000));
  echoClientHelper.SetAttribute ("Interval", TimeValue (Seconds (0.5)));
  echoClientHelper.SetAttribute ("PacketSize", UintegerValue (250));
  echoClientHelper.SetAttribute ("RemotePort", UintegerValue (remUePort));

 

    singleClientApp = echoClientHelper.Install (remoteUeNodes.Get (u));
    // Ipv6Address address = remoteHost->GetObject<Ipv6>()->GetAddress(1,1).GetAddress();

      UdpEchoClientHelper client (address);
      // UdpClientHelper client (address, otherPort);
      client.SetAttribute ("Interval", TimeValue (Seconds(emergencyIntervalSeconds->GetValue())));
      client.SetAttribute ("MaxPackets", UintegerValue(100000));
      client.SetAttribute("PacketSize", UintegerValue(40));
      client.SetAttribute ("RemotePort", UintegerValue (otherPort));
      // client.SetPriority(0);

      emClientApp = client.Install (remoteUeNodes.Get(u));
       //Start the application 3.0 s after the remote UE started the relay service
      //normally it should be enough time to connect
      singleClientApp.Start (Seconds (3 + u*0.060  ) );
      //Stop the application after 10.0 s
      singleClientApp.Stop (Seconds (3 + u*0.060+ 30.0));

      emClientApp.Start (Seconds (3 + u*0.060 ) );
      // Stop the application after 10.0 s
      emClientApp.Stop (Seconds (3 + u*0.060+ 30.0));

      clientApps.Add (emClientApp);
  
  NS_LOG_INFO ("Remote UE node id = [" << remoteUeNodes.Get (u)->GetId () << "] start application at " << Seconds (3.0+u*0.04) << " s");
             

    oss << "tx\tC\t"  <<"\t"<< remoteUeNodes.Get (u)->GetDevice (0)->GetObject<LteUeNetDevice> ()->GetImsi ();
  singleClientApp.Get (0)->TraceConnect ("TxWithAddresses", oss.str (), MakeBoundCallback (&UePacketInputTrace, packetInputStream));
  oss.str ("");

  oss << "rx\tC\t" <<"\t"<< remoteUeNodes.Get (u)->GetDevice (0)->GetObject<LteUeNetDevice> ()->GetImsi ();
  singleClientApp.Get (0)->TraceConnect ("RxWithAddresses", oss.str (), MakeBoundCallback (&UePacketTrace, packetOutputStream));
  oss.str ("");


    oss << "em tx\tC\t"  <<"\t"<<remoteUeNodes.Get (u)->GetDevice (0)->GetObject<LteUeNetDevice> ()->GetImsi ();
  emClientApp.Get (0)->TraceConnect ("TxWithAddresses", oss.str (), MakeBoundCallback (&UePacketInputTraceEm, packetInputStreamemTest));
  oss.str ("");

  oss << "em rx\tC\t" <<"\t"<<remoteUeNodes.Get (u)->GetDevice (0)->GetObject<LteUeNetDevice> ()->GetImsi ();
  emClientApp.Get (0)->TraceConnect ("RxWithAddresses", oss.str (), MakeBoundCallback (&UePacketTraceEm, packetOutputStreamemTest));
  oss.str ("");
clientApps.Add (singleClientApp);
       

  }
  
  

  ///*** Configure Relaying ***///

  bool useRelay = true;
  std::cout<<"Check Value: "<< useRelay<< std::endl;
  bool threshold_based_selection = true;



  if (useRelay)
    {
      // bool r = relaying;
      std::cout << "Use value:"<<useRelay <<std::endl;
      std::cout << "Why I am in this???"<<std::endl; 
      proseHelper->SetIpv6BaseForRelayCommunication ("7777:f00e::", Ipv6Prefix (48));
      //Setup dedicated bearer for the Relay UE
      Ptr<EpcTft> tft = Create<EpcTft> ();
      EpcTft::PacketFilter dlpf;
      // dlpf.localIpv6Address.Set ("7777:f00e::");
      // dlpf.localIpv6Prefix = Ipv6Prefix (32);
      dlpf.localIpv6Address = proseHelper->GetIpv6NetworkForRelayCommunication ();
      dlpf.localIpv6Prefix = proseHelper->GetIpv6PrefixForRelayCommunication ();
      tft->Add (dlpf);
      EpsBearer bearer (EpsBearer::NGBR_VIDEO_TCP_DEFAULT);
      lteHelper->ActivateDedicatedEpsBearer (relayUeDevs, bearer, tft);

      if (threshold_based_selection)
      {

      Ptr<OutputStreamWrapper> RsrpMeasurementsStream_relay = ascii.CreateFileStream ("RsrpMeasurementTrace_relayUe.txt");
      *RsrpMeasurementsStream_relay->GetStream () << "time(s)\timsi\tcellId\trsrp\tthreshold" << std::endl;
        for (uint32_t u=0; u< relayUeNodes.GetN(); ++u)
        {
          // sleep((u*0.1+0.32));
          std::ostringstream ctx;
          ctx << relayUeNodes.Get (u)->GetId ();
          Ptr<LteUeRrc> rrc = relayUeDevs.Get (u)->GetObject<LteUeNetDevice> ()->GetRrc ();
          PointerValue ptrOne;
          rrc->TraceConnect ("RsrpMeasurement",  ctx.str (),
                           MakeBoundCallback (&RsrpMeasurementTrace_relayUe,
                                              RsrpMeasurementsStream_relay,
                                              threshold_relay,
                                              relayUeDevs.Get (u)->GetObject<LteUeNetDevice> ()));
        }
      }
      else
      {
        for (uint32_t u=0; u< relayUeNodes.GetN(); u++)
        {
          std::cout << "Why I am in this??? why???"<<std::endl; 
          //Schedule the start of the relay service in the Relay UE
          Simulator::Schedule (Seconds (1+0.32+0.32+ 0.32+u*(0.01)), &LteSidelinkHelper::StartRelayService,
                           proseHelper, relayUeDevs.Get (u), SERVICE_CODE,
                           LteSlUeRrc::ModelA, LteSlUeRrc::RelayUE);
          NS_LOG_INFO ("Relay UE node id = [" << relayUeNodes.Get (u)->GetId () << "] provides Service Code " << SERVICE_CODE << " and start service at " << u+2.0 << " s");
        }
      }




      //Connect trace in all UEs for monitoring UE-to-Network Relay signaling messages
      Ptr<OutputStreamWrapper> PC5SignalingPacketTraceStream = ascii.CreateFileStream ("PC5SignalingPacketTrace_remoteUE.txt");
      *PC5SignalingPacketTraceStream->GetStream () << "time(s)\ttxId\tRxId\tmsgType" << std::endl;
      for (uint32_t ueDevIdx = 0; ueDevIdx < allUeDevs.GetN (); ueDevIdx++)
        {
          // std::cout << "I am here in the factory code"<< std::endl; 
          Ptr<LteUeRrc> rrc = allUeDevs.Get (ueDevIdx)->GetObject<LteUeNetDevice> ()->GetRrc ();
          PointerValue ptrOne;
          rrc->GetAttribute ("SidelinkConfiguration", ptrOne);
          Ptr<LteSlUeRrc> slUeRrc = ptrOne.Get<LteSlUeRrc> ();
          slUeRrc->TraceConnectWithoutContext ("PC5SignalingPacketTrace",
                                               MakeBoundCallback (&TraceSinkPC5SignalingPacketTrace,
                                                                  PC5SignalingPacketTraceStream));


        }

            //Connect trace in the Remote UE to monitor primary cell RSRP and activate
      //Relay service when it falls below 'threshold' (See 'RsrpMeasurementTrace' function)
      Ptr<OutputStreamWrapper> RsrpMeasurementsStream = ascii.CreateFileStream ("RsrpMeasurementTrace_remoteUe.txt");
      *RsrpMeasurementsStream->GetStream () << "time(s)\timsi\tcellId\trsrp\tthreshold" << std::endl;
      for (uint32_t u=0; u< remoteUeNodes.GetN(); ++u)
      {
        // sleep((u*0.32+0.32));
      std::ostringstream ctx;
      ctx << remoteUeNodes.Get (u)->GetId ();
      Ptr<LteUeRrc> rrc = remoteUeDevs.Get (u)->GetObject<LteUeNetDevice> ()->GetRrc ();
      PointerValue ptrOne;
      rrc->TraceConnect ("RsrpMeasurement",  ctx.str (),
                         MakeBoundCallback (&RsrpMeasurementTrace,
                                            RsrpMeasurementsStream,
                                            threshold,
                                            remoteUeDevs.Get (u)->GetObject<LteUeNetDevice> ()));
      }
      //Tracing SD-RSRP measurements in the Remote UEs
  Ptr<OutputStreamWrapper> ReportUeSdRsrpMeasurementsStream = ascii.CreateFileStream ("ReportUeSdRsrpMeasurementsTrace_remoteUe.txt");
  *ReportUeSdRsrpMeasurementsStream->GetStream () << "time(s)\trnti\trelayId\tSC\tsdRsrp" << std::endl;
  for (uint32_t ueDevIdx = 0; ueDevIdx < remoteUeDevs.GetN (); ueDevIdx++)
    {
      // int32_t imsi;
      Ptr<LteUePhy> phy = remoteUeDevs.Get (ueDevIdx)->GetObject<LteUeNetDevice> ()->GetPhy ();
      std::ostringstream ctx;
      ctx << remoteUeNodes.Get (ueDevIdx)->GetId ();
      Ptr<LteUeRrc> rrc = remoteUeDevs.Get (ueDevIdx)->GetObject<LteUeNetDevice> ()->GetRrc ();
      // imsi= remoteUeNodes.Get (ueDevIdx)->GetDevice (0)->GetObject<LteUeNetDevice> ()->GetImsi ();
      // std::cout<<"IMSI: "<< imsi<<std::endl;

      phy->TraceConnectWithoutContext ("ReportUeSdRsrpMeasurements",
                                       MakeBoundCallback (&ReportUeSdRsrpMeasurementsTrace,
                                                          ReportUeSdRsrpMeasurementsStream,
                                                          remoteUeDevs.Get (ueDevIdx)->GetObject<LteUeNetDevice> ()));
    }

  //Tracing Relay Selection decisions in the Remote UEs
  Ptr<OutputStreamWrapper> RelayUeSelectionStream = ascii.CreateFileStream ("RelayUeSelectionTrace_remoteUe.txt");
  *RelayUeSelectionStream->GetStream () << "time(s)\timsi\tSC\tcRelayId\tsRelayId" << std::endl;

  for (uint32_t ueDevIdx = 0; ueDevIdx < remoteUeDevs.GetN (); ueDevIdx++)
    {
      Ptr<LteUeRrc> rrc = remoteUeDevs.Get (ueDevIdx)->GetObject<LteUeNetDevice> ()->GetRrc ();
      PointerValue ptrOne;
      rrc->GetAttribute ("SidelinkConfiguration", ptrOne);
      Ptr<LteSlUeRrc> slUeRrc = ptrOne.Get<LteSlUeRrc> ();
      PointerValue ptrTwo;
      slUeRrc->GetAttribute ("SlController", ptrTwo);
      Ptr<LteSlBasicUeController> ctlr = ptrTwo.Get<LteSlBasicUeController>();
      ctlr->TraceConnectWithoutContext ("RelayUeSelection",
                                        MakeBoundCallback (&RelayUeSelectionTrace,
                                                           RelayUeSelectionStream));
    }

  //Tracing one-to-one connection status in all UEs (Remote UEs and Relay UEs)
  Ptr<OutputStreamWrapper> Pc5ConnectionStatusStream = ascii.CreateFileStream ("Pc5ConnectionStatusTrace_remoteUe.txt");
  *Pc5ConnectionStatusStream->GetStream () << "time(s)\tselfUeId\tpeerUeId\trole\tstatus\tPosition\t Difference" << std::endl;

  for (uint32_t ueDevIdx = 0; ueDevIdx < allUeDevs.GetN (); ueDevIdx++)
    {
      // std::cout << "I am here in the factory code again"<< std::endl; 
      Ptr<LteUeRrc> rrc = allUeDevs.Get (ueDevIdx)->GetObject<LteUeNetDevice> ()->GetRrc ();
      PointerValue ptrOne;
      rrc->GetAttribute ("SidelinkConfiguration", ptrOne);
      Ptr<LteSlUeRrc> slUeRrc = ptrOne.Get<LteSlUeRrc> ();
      PointerValue ptrTwo;
      slUeRrc->GetAttribute ("SlController", ptrTwo);
      Ptr<LteSlBasicUeController> ctlr = ptrTwo.Get<LteSlBasicUeController>();
      ctlr->TraceConnectWithoutContext ("Pc5ConnectionStatus",
                                        MakeBoundCallback (&Pc5ConnectionStatusTrace,
                                                           Pc5ConnectionStatusStream));
    }
  

 


    }

    Ptr<OutputStreamWrapper> stream1 = ascii.CreateFileStream ("Ueimsi_remoteUe.txt");
    Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/ConnectionEstablished", MakeBoundCallback (&NotifyConnectionEstablishedEnb,stream1));
      Ptr<OutputStreamWrapper> RSRPStream = ascii.CreateFileStream ("RSRP_position_remoteUe.txt");
     
       Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/RsrpMeasurement", MakeBoundCallback (&AllRsrpMeasurementTrace,RSRPStream));

      Ptr<OutputStreamWrapper> theStream = ascii.CreateFileStream ("Drop_remoteUe.txt");
      // Ptr<Ipv6L3Protocol> ipv6L3Protocol = ipv6h->GetObject<Ipv6L3Protocol> ();
      // ipv6L3Protocol->TraceConnectWithoutContext ("Drop", MakeBoundCallback (&Ipv6L3ProtocolDrop, theStream));

        // oss << "/NodeList/" << remoteHost->GetId () << "/$ns3::Ipv6L3Protocol/Drop";
      Config::Connect ("/NodeList/*/$ns3::Ipv6L3Protocol/Drop", MakeBoundCallback (&Ipv6L3ProtocolDrop, theStream));
  NS_LOG_INFO ("Enabling traces...");
        //check wheather the packet is dropped at the physical layer
    internetDevices.Get (1)->TraceConnectWithoutContext("PhyRxDrop", MakeCallback (&RxDrop));


  lteHelper->EnableTraces();
  //Transmission traces
lteHelper->EnableSlPscchMacTraces ();
lteHelper->EnableSlPsschMacTraces ();
lteHelper->EnableSlPsdchMacTraces ();
//Reception traces
lteHelper->EnableSlRxPhyTraces ();
lteHelper->EnableSlPscchRxPhyTraces ();
lteHelper->EnableDiscoveryMonitoringRrcTraces ();

  Ptr<FlowMonitor> flowMonitor;
  FlowMonitorHelper flowHelper;
  flowMonitor = flowHelper.InstallAll();

  NS_LOG_INFO ("Simulation time " << simTime << " s");
  NS_LOG_INFO ("Starting simulation...");

  Simulator::Stop (Seconds (simTime));
  AnimationInterface anim("factory_relaying.xml");
    for (uint32_t i = 0; i < remoteUeNodes.GetN (); ++i)
    {
      anim.UpdateNodeDescription (remoteUeNodes.Get (i), "Mobile Nodes"); // Optional
      anim.UpdateNodeColor (remoteUeNodes.Get (i), 255, 0, 0); // Optional
    }
  for (uint32_t i = 0; i < relayUeNodes.GetN (); ++i)
    {
      anim.UpdateNodeDescription (relayUeNodes.Get (i), "Static Nodes"); // Optional
      anim.UpdateNodeColor (relayUeNodes.Get (i), 0, 255, 0); // Optional
    }
  for (uint32_t i = 0; i < enbNode.GetN (); ++i)
    {
      anim.UpdateNodeDescription (enbNode.Get (i), "eNB"); // Optional
      anim.UpdateNodeColor (enbNode.Get (i), 0, 0, 255); // Optional 
    }
  anim.EnablePacketMetadata (true);
  Simulator::Run ();

    std::string outputDir = "./";
    std::string simTag= "relaying_remoteUe";
    flowMonitor->CheckForLostPackets ();
    Ptr<Ipv6FlowClassifier> classifier = DynamicCast<Ipv6FlowClassifier> (flowHelper.GetClassifier6 ());
    // FlowMonitor::FlowStatsContainer stats = flowMonitor->GetFlowStats ();
    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

    double averageFlowThroughput = 0.0;
    double averageFlowDelay = 0.0;
    uint32_t sumLostPackets = 0.0;
    uint32_t sumTxPackets = 0.0;
    uint32_t delayPacketSum = 0.0; 
    double delayDrop = 0.0; 

    std::ofstream outFile;
    std::string filename = outputDir + "/" + simTag;
    outFile.open (filename.c_str (), std::ofstream::out | std::ofstream::app);
    if (!outFile.is_open ())
    {
      std::cout<<"Can't open file " << filename;
      return 1;
    }
    outFile.setf (std::ios_base::fixed);

  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
      
      Ipv6FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
      std::stringstream protoStream;
      protoStream << (uint16_t) t.protocol;
      if (t.protocol == 6)
        {
          protoStream.str ("TCP");
        }
      if (t.protocol == 17)
        {
          protoStream.str ("UDP");
        }

        outFile << "Flow " << i->first << " (" << t.sourceAddress << ":" << t.sourcePort << " -> " << t.destinationAddress << ":" << t.destinationPort << ") proto " << protoStream.str () << "\n"; 
        outFile << "  Tx Packets: " << i->second.txPackets << "\n";
        outFile << "  Tx Bytes:   " << i->second.txBytes << "\n";
        outFile << "  TxOffered:  " << i->second.txBytes * 8.0 / (simTime - 0.01) / 1000 / 1000  << " Mbps\n";
        outFile << "  Rx Bytes:   " << i->second.rxBytes << "\n";
        outFile << "  Lost Packets: " << i->second.lostPackets<< "\n";
        outFile << "  Packet Forwards: "<< i->second.timesForwarded<<"\n";
        // outFile << " Packets Dropped : " << i-> second.packetsDropped<<"\n";
        // outFile << " Byte Dropped: " << i->second.bytesDropped<<"\n";
        sumTxPackets += i->second.txPackets;
        sumLostPackets+= i->second.lostPackets;
      if (i->second.rxPackets > 0)
        {
          // Measure the duration of the flow from receiver's perspective
          double rxDuration = i->second.timeLastRxPacket.GetSeconds () - i->second.timeFirstTxPacket.GetSeconds ();

          averageFlowThroughput += i->second.rxBytes * 8.0 / rxDuration / 1000 / 1000;
          averageFlowDelay += 1000 * i->second.delaySum.GetSeconds () / i->second.rxPackets;
          delayDrop = i->second.lastDelay.GetSeconds(); 
          std::cout << "Delay per packet"<< delayDrop; 
        if (delayDrop > 10)
        {
          delayPacketSum ++; 

        }

        uint32_t packetsDroppedByQueueDisc = 0;
        uint64_t bytesDroppedByQueueDisc = 0;
        if (stats[1].packetsDropped.size () > Ipv6FlowProbe::DROP_QUEUE_DISC)
        {
        packetsDroppedByQueueDisc = stats[1].packetsDropped[Ipv6FlowProbe::DROP_QUEUE_DISC];
        bytesDroppedByQueueDisc = stats[1].bytesDropped[Ipv6FlowProbe::DROP_QUEUE_DISC];
        }
        std::cout << "  Packets/Bytes Dropped by Queue Disc:   " << packetsDroppedByQueueDisc
        << " / " << bytesDroppedByQueueDisc << std::endl;
        uint32_t packetsDroppedByNetDevice = 0;
        uint64_t bytesDroppedByNetDevice = 0;
        if (stats[1].packetsDropped.size () > Ipv6FlowProbe::DROP_QUEUE)
        {
        packetsDroppedByNetDevice = stats[1].packetsDropped[Ipv6FlowProbe::DROP_QUEUE];
        bytesDroppedByNetDevice = stats[1].bytesDropped[Ipv6FlowProbe::DROP_QUEUE];
        }
        std::cout << "  Packets/Bytes Dropped by NetDevice:   " << packetsDroppedByNetDevice
        << " / " << bytesDroppedByNetDevice << std::endl;



          outFile << "  Throughput: " << i->second.rxBytes * 8.0 / rxDuration / 1000 / 1000  << " Mbps\n";
          outFile << "  Mean delay:  " << 1000 * i->second.delaySum.GetSeconds () / i->second.rxPackets << " ms\n";
          //outFile << "  Mean upt:  " << i->second.uptSum / i->second.rxPackets / 1000/1000 << " Mbps \n";
          outFile << "  Mean jitter:  " << 1000 * i->second.jitterSum.GetSeconds () / i->second.rxPackets  << " ms\n";


        }
      else
        {
          outFile << "  Throughput:  0 Mbps\n";
          outFile << "  Mean delay:  0 ms\n";
          outFile << "  Mean upt:  0  Mbps \n";
          outFile << "  Mean jitter: 0 ms\n";
        }
      outFile << "\n" << "Loss Packet %" <<(((i->second.txPackets-i->second.rxPackets)*1.0)/i->second.txPackets);
      // outFile << "\n" << "Lost Packets:" << sumLostPackets;
      outFile << "  Rx Packets: " << i->second.rxPackets << "\n";

    }
    double avgDelay = averageFlowDelay / stats.size () ;

    outFile << "\n" << "Packets Transmitted" << sumTxPackets << "\n";
    outFile << "\n" << "Packets Lost" << sumLostPackets << "\n";
    outFile << "\n" << "Packet Lost due to delay"<< delayPacketSum << "\n";
    
    outFile << "\n\n  Mean flow throughput: " << averageFlowThroughput / stats.size() << "\n";
    outFile << "  Mean flow delay: " << avgDelay << "\n";
    outFile <<"----------------------------------------Next Simulation -------------------------------------------------------------------------------------" << "\n";
  outFile.close ();

  /*GtkConfigStore config;
  config.ConfigureAttributes();*/

  flowMonitor->SerializeToXmlFile("relaying_remoteUe.xml", true, true);
  Simulator::Destroy ();

 
  results.open ("Results.txt", std::ofstream::out | std::ofstream::app);
  results<<relaySelection<<"\t"<<runValue<<"\t"<<threshold << "\t"<<useRelay
          <<"\t"<<threshold_based_selection<<"\t"<<threshold_relay<<"\t"<<nRelayUes
          <<"\t"<<nRemoteUes<<"\t"<<RX_PACKETS<<"\t"<<TX_PACKETS<<"\t"<<RXem_PACKETS<<"\t"<<TXem_PACKETS<<"\t"
          <<avgDelay<<"\t algorithm compare"<<std::endl;

  results.close();



  std::cout << "Rx packets: " << RX_PACKETS << "/" << TX_PACKETS << "\t"<< active_relay<<"Em packets: " << RXem_PACKETS << "/" << TXem_PACKETS<< std::endl;

  return 0;

}
