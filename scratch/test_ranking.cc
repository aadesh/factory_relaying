/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * NIST-developed software is provided by NIST as a public
 * service. You may use, copy and distribute copies of the software in
 * any medium, provided that you keep intact this entire notice. You
 * may improve, modify and create derivative works of the software or
 * any portion of the software, and you may copy and distribute such
 * modifications or works. Modified works should carry a notice
 * stating that you changed the software and should note the date and
 * nature of any such change. Please explicitly acknowledge the
 * National Institute of Standards and Technology as the source of the
 * software.
 *
 * NIST-developed software is expressly provided "AS IS." NIST MAKES
 * NO WARRANTY OF ANY KIND, EXPRESS, IMPLIED, IN FACT OR ARISING BY
 * OPERATION OF LAW, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
 * NON-INFRINGEMENT AND DATA ACCURACY. NIST NEITHER REPRESENTS NOR
 * WARRANTS THAT THE OPERATION OF THE SOFTWARE WILL BE UNINTERRUPTED
 * OR ERROR-FREE, OR THAT ANY DEFECTS WILL BE CORRECTED. NIST DOES NOT
 * WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OF THE
 * SOFTWARE OR THE RESULTS THEREOF, INCLUDING BUT NOT LIMITED TO THE
 * CORRECTNESS, ACCURACY, RELIABILITY, OR USEFULNESS OF THE SOFTWARE.
 *
 * You are solely responsible for determining the appropriateness of
 * using and distributing the software and you assume all risks
 * associated with its use, including but not limited to the risks and
 * costs of program errors, compliance with applicable laws, damage to
 * or loss of data, programs or equipment, and the unavailability or
 * interruption of operation. This software is not intended to be used
 * in any situation where a failure could cause risk of injury or
 * damage to property. The software developed by NIST employees is not
 * subject to copyright protection within the United States.
 */


#include "ns3/lte-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store.h"
#include <cfloat>
#include <sstream>
#include <math.h>
#include "ns3/gnuplot.h"
#include "ns3/udp-echo-client.h"
#include <ns3/buildings-helper.h>
#include <ns3/buildings-module.h>
#include "ns3/flow-monitor-module.h"
#include "ns3/flow-monitor-helper.h"


using namespace ns3;

/*
 * This scenario show the activation of UE-to-Network Relay service on the
 * Remote UE depending on the RSRP measurements of the primary cell.
 *
 * The topology:
 *                                                        .-------------.  -
 *                                                        |             |  |
 *                             Relay         Remote       |  Building   | 200 m
 *    eNodeB                    UE             UE         |             |  |
 *                                                        '-------------'  -
 *     |------------------------||-------------||--------||-------------|
 *                400 m               50 m         10 m          200 m
 *
 *                       Remote UE mobility:    >--------->-------->
 *                       Approx. time:          |---10 s--||--5 s--|
 *
 *                       Application ON time:        |----10 s-----|
 *
 * The Remote UE moves towards  the building at a constant speed of 1 m/s.
 * crossing the 'edge' of the building at approximately 10.0 s.
 * The Remote UE starts its application at 5.0 s sending one packet every 0.5 s,
 * and stops at 15.0 s, for a total of 20 sent packets towards a server in the
 * network. The server echoes back the packets to the Remote UE and we trace
 * those received packets.
 *
 * Two parameters can be configured on the command line:
 * - useRelay
 * - threshold.
 * When 'useRelay' is set to false the Remote UE does not use the Relay UE,
 * i.e., it uses the network directly and its service degrades when entering
 * into the building due to channel degradation, which is reflected in packet
 * loss.
 * When 'useRelay' is set to true, the Remote UE activates the UE-to-Network
 * Relay service when the RSRP of the primary cell falls below 'threshold'.
 * Depending on the value of 'threshold' we see that the packet loss could be
 * mitigated with the use of the UE-to-Network Relay UE in this scenario.
 *
 * The total number of received packets are displayed using the standard output
 * upon completion of the simulation.
 *
 * Given the default configuration in this script, the following are example
 * values for the parameters and the expected output
 * .----------------------------.-------------.
 * |        Parameters          |   Output    |
 * |----------.-----------------.-------------|
 * | useRelay | threshold (dBm) | Rx Packets  |
 * |----------.-----------------.-------------|
 * | false    | -100 (not used) |   10/20     | // Remote UE does not activate Relay Service
 * |----------.-----------------.-------------|
 * | true     | -100            |   20/20     | // Remote UE activates Relay Service at the beginning of the simulation (the RSRP is already below -100 dBm in the initial position)
 * |----------.-----------------.-------------|
 * | true     | -120            |   16/20     | // Remote UE activates Relay Service when 'entering' to the building (the RSRP decays below -120 dBm when entering to the building, i.e., after 10 s).
 * |----------.-----------------.-------------|
 * | true     | -127            |   12/20     | // Remote UE activates Relay Service when inside of the building (the RSRP decays below -127 dBm after 12 s).
 * '------------------------------------------'
 *
 * Additionally, users can inspect the generated output files to gain more
 * insights about the simulation:
 * - AppPacketTrace.txt: Log of received packets by the Remote UE at the
 *                       application layer
 * - MobilityTrace.txt: Log of the position and velocity of the Remote UE
 * - PC5SignalingPacketTrace.txt: Log of the received PC5 signaling messages by
 *                                both UEs
 * - RsrpMeasurementTrace.txt: Log of primary cell RSRP monitoring by the
 *                             Remote UE
 * - UlMacStats.txt: MAC layer statistics for the UL
 * - UlPdcpStats.txt PDCP layer statistics for the UL
 * - DlMacStats.txt: MAC layer statistics for the DL
 * - DlPdcpStats.txt: PDCP layer statistics for the DL
 * E.g., users can follow the movement of the Remote UE in 'MobilityTrace.txt',
 * and see when the RSRP crosses the 'threshold' in 'RsrpMeasurementTrace.txt'.
 * Then, observe that the one-to-one communication setup procedure was
 * performed between the Remote UE and the Relay UE by checking the
 * corresponding PC5 message exchange in 'PC5SignalingPacketTrace.txt'.
 * Finally, users can verify which UE is using the network (UL/DL) to send and
 * receive packets at any given point of the simulation in the last four listed
 * output files. When 'useRelay' parameter is set to true, the Remote UE uses
 * the network before connection to the Relay UE. After connection, the Relay UE
 * is the one using the network to transmit and receive the packets that it
 * is relaying for the Remote UE.
 */


NS_LOG_COMPONENT_DEFINE ("Ranking");

// uint32_t SERVICE_CODE = 33;
int m_txPacketNum= 0;
std::list <Ipv6Address> m_addressList; ///< addressList

int m_rxPacketNum= 0; //Used to trace the number of Rx packets


void
DataPacketSinkTxNode (Ptr<const Packet> p, const Address &src, const Address &dst)
{
  m_txPacketNum++;
}

void
DataPacketSinkRxNode (Ptr<const Packet> p, const Address &src, const Address &dst)
{
  m_rxPacketNum++;
  m_addressList.push_back (Inet6SocketAddress::ConvertFrom (src).GetIpv6 ());
}

// void
// UePacketTrace (Ptr<OutputStreamWrapper> stream, std::string context, Ptr<const Packet> p, const Address &srcAddrs, const Address &dstAddrs)
// {
//   std::ostringstream oss;
//   stream->GetStream ()->precision (6);

//   *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
//                         << context << "\t"
//                         << Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 () << ":"
//                         << Inet6SocketAddress::ConvertFrom (srcAddrs).GetPort () << "\t"
//                         << Inet6SocketAddress::ConvertFrom (dstAddrs).GetIpv6 () << ":"
//                         << Inet6SocketAddress::ConvertFrom (dstAddrs).GetPort () << "\t"
//                         << p->GetSize () << "\t"
//                         << std::endl;
//                         m_addressList.push_back (Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 ());
//   RX_PACKETS++;
// }

// void
// UePacketInputTrace (Ptr<OutputStreamWrapper> stream, std::string context, Ptr<const Packet> p, const Address &srcAddrs, const Address &dstAddrs)
// {
//   std::ostringstream oss;
//   stream->GetStream ()->precision (6);

//   *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
//                         << context << "\t"
//                         << Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 () << ":"
//                         << Inet6SocketAddress::ConvertFrom (srcAddrs).GetPort () << "\t"
//                         << Inet6SocketAddress::ConvertFrom (dstAddrs).GetIpv6 () << ":"
//                         << Inet6SocketAddress::ConvertFrom (dstAddrs).GetPort () << "\t"
//                         << p->GetSize () << "\t"
//                         << std::endl;
//   TX_PACKETS++;
// }

// /*
//  * Trace sink function for logging when a packet is transmitted or received
//  * at the application layer
//  */
// void
// UePacketTrace (Ptr<OutputStreamWrapper> stream, std::string context, Ptr<const Packet> p, const Address &srcAddrs, const Address &dstAddrs)
// {
//   std::ostringstream oss;
//   stream->GetStream ()->precision (6);

//   *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
//                         << context << "\t"
//                         << Inet6SocketAddress::ConvertFrom (srcAddrs).GetIpv6 () << ":"
//                         << Inet6SocketAddress::ConvertFrom (srcAddrs).GetPort () << "\t"
//                         << Inet6SocketAddress::ConvertFrom (dstAddrs).GetIpv6 () << ":"
//                         << Inet6SocketAddress::ConvertFrom (dstAddrs).GetPort () << "\t"
//                         << p->GetSize () << "\t"
//                         << std::endl;
//   RX_PACKETS++;
// }

// /*
//  * Trace sink function to monitor primary cell RSRP and activate UE-to-Network
//  * Relay service as Remote UE when the RSRP falls below a threshold
//  */
// void
// RsrpMeasurementTrace (Ptr<OutputStreamWrapper> stream, const double threshold, Ptr<NetDevice> ueDevice, std::string context, uint64_t imsi, uint16_t cellId, double rsrp)
// {
//   std::ostringstream oss;
//   stream->GetStream ()->precision (6);
//   *stream->GetStream () << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t"
//                         << imsi << "\t"
//                         << cellId << "\t"
//                         << rsrp << "\t"
//                         << threshold << "\t"
//                         << std::endl;

//   Ptr<LteUeRrc> rrc = ueDevice->GetObject<LteUeNetDevice> ()->GetRrc ();
//   PointerValue ptrOne;
//   rrc->GetAttribute ("SidelinkConfiguration", ptrOne);
//   Ptr<LteSlUeRrc> slUeRrc = ptrOne.Get<LteSlUeRrc> ();

//   //Avoid that in-coverage Remote UE starts Relay service before cell
//   //attachment (happening at the first RSRP report: 200 ms)

//   if (Simulator::Now ().GetMilliSeconds () < 400)
//     {
//       return;
//     }
//   else
//     {
//       //If the RSRP of the primary cell is below the threshold and
//       //the UE is not in Remote UE role already (i.e., already monitoring Discovery Relay Announcements (we are using discovery Model A))
//       //then start UE-to-Network Relay Service as Remote UE
//       if (rsrp < threshold && !slUeRrc->IsMonitoringRelayServiceCode (LteSlDiscHeader::DISC_RELAY_ANNOUNCEMENT, SERVICE_CODE))
//         {
//           rrc->StartRelayService (SERVICE_CODE, LteSlUeRrc::ModelA, LteSlUeRrc::RemoteUE);
//         }
//     }
// }

// /*
//  * Trace sink function for logging when PC5 signaling messages are received
//  */
void
TraceSinkPC5SignalingPacketTrace (Ptr<OutputStreamWrapper> stream, uint32_t srcL2Id, uint32_t dstL2Id, Ptr<Packet> p)
{
  LteSlPc5SignallingMessageType lpc5smt;
  p->PeekHeader (lpc5smt);
  *stream->GetStream () << Simulator::Now ().GetSeconds () << "\t" << srcL2Id << "\t" << dstL2Id << "\t" << lpc5smt.GetMessageName () << std::endl;
}


int main (int argc, char *argv[])
{

  double simTime = 50; //Total duration of the simulation [s]
 
  double relayUeInitXPos = 25.0; //Initial X coordinate of the relay UE
  double remoteUeInitXPos = 50.0; //Initial X coordinate of the remote UE
  uint32_t nRelayUes = 2; //Number of relay UEs
  uint32_t nRemoteUes = 1; //Number of remote UEs
  bool remoteTraffic = true; //The Remote UEs have their own traffic
  bool relayTraffic = false; //The Relay UEs have their own traffic

  NS_LOG_INFO ("Configuring default parameters...");

  //Configure the UE for UE_SELECTED scenario
  Config::SetDefault ("ns3::LteUeMac::SlGrantMcs", UintegerValue (16));
  Config::SetDefault ("ns3::LteUeMac::SlGrantSize", UintegerValue (6)); //The number of RBs allocated per UE for Sidelink
  Config::SetDefault ("ns3::LteUeMac::Ktrp", UintegerValue (1));
  Config::SetDefault ("ns3::LteUeMac::UseSetTrp", BooleanValue (false)); //use default Trp index of 0
  Config::SetDefault ("ns3::LteUeMac::SlScheduler", StringValue ("MaxCoverage")); 
  // //Set the frequency
  // Config::SetDefault ("ns3::LteEnbNetDevice::DlEarfcn", UintegerValue (5330));
  // Config::SetDefault ("ns3::LteUeNetDevice::DlEarfcn", UintegerValue (5330));
  // Config::SetDefault ("ns3::LteEnbNetDevice::UlEarfcn", UintegerValue (23330));
  // Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue (50));
  // Config::SetDefault ("ns3::LteEnbNetDevice::UlBandwidth", UintegerValue (50));

    // //Set the frequency
  Config::SetDefault ("ns3::LteEnbNetDevice::DlEarfcn", UintegerValue (100));
  Config::SetDefault ("ns3::LteUeNetDevice::DlEarfcn", UintegerValue (100));
  Config::SetDefault ("ns3::LteEnbNetDevice::UlEarfcn", UintegerValue (18100));
  Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue (50));
  Config::SetDefault ("ns3::LteEnbNetDevice::UlBandwidth", UintegerValue (50));

  // Set error models
  Config::SetDefault ("ns3::LteSpectrumPhy::SlCtrlErrorModelEnabled", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::SlDataErrorModelEnabled", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlFullDuplexEnabled", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::DropRbOnCollisionEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteUePhy::DownlinkCqiPeriodicity", TimeValue (MilliSeconds (79)));


  //Set the UEs power in dBm
  Config::SetDefault ("ns3::LteUePhy::TxPower", DoubleValue (23.0));
  //Set the eNBs power in dBm
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (46.0));

  //Sidelink bearers activation time
  Time slBearersActivationTime = Seconds (2.0);

  //Create and set the helpers
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<PointToPointEpcHelper>  epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  Ptr<LteSidelinkHelper> proseHelper = CreateObject<LteSidelinkHelper> ();
  proseHelper->SetLteHelper (lteHelper);
  Config::SetDefault ("ns3::LteSlBasicUeController::ProseHelper", PointerValue (proseHelper));

  //Set pathloss model
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::Hybrid3gppPropagationLossModel"));

  //Enable Sidelink
  lteHelper->SetAttribute ("UseSidelink", BooleanValue (true));

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create the Internet
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);

  //Create nodes (eNb + UEs)
  NodeContainer enbNode;
  enbNode.Create (1);
  NS_LOG_INFO ("eNb node id = [" << enbNode.Get (0)->GetId () << "]");

  NodeContainer relayUeNodes;
  relayUeNodes.Create (nRelayUes);

  NodeContainer remoteUeNodes;
  remoteUeNodes.Create (nRemoteUes);
  for (uint32_t ry = 0; ry < relayUeNodes.GetN (); ry++)
    {
      NS_LOG_INFO ("Relay UE " << ry + 1 << " node id = [" << relayUeNodes.Get (ry)->GetId () << "]");
    }
  for (uint32_t rm = 0; rm < remoteUeNodes.GetN (); rm++)
    {
      NS_LOG_INFO ("Remote UE " << rm + 1 << " node id = [" << remoteUeNodes.Get (rm)->GetId () << "]");
    }
  NodeContainer allUeNodes = NodeContainer (relayUeNodes,remoteUeNodes);

  //Position of the nodes
  //eNodeB
  Ptr<ListPositionAllocator> positionAllocEnb = CreateObject<ListPositionAllocator> ();
  positionAllocEnb->Add (Vector (0.0, 0.0, 30.0));

  //Relay UEs
  Ptr<ListPositionAllocator> positionAllocRelays = CreateObject<ListPositionAllocator> ();
  for (uint32_t ry = 0; ry < relayUeNodes.GetN (); ++ry)
    {
      positionAllocRelays->Add (Vector (relayUeInitXPos, 0.0, 1.5));
    }
  //Remote UEs
  Ptr<ListPositionAllocator> positionAllocRemotes = CreateObject<ListPositionAllocator> ();
  for (uint32_t rm = 0; rm < remoteUeNodes.GetN (); ++rm)
    {
      positionAllocRemotes->Add (Vector (remoteUeInitXPos, 0.0, 1.5));

    }

  //Install mobility
  //eNodeB
  MobilityHelper mobilityeNodeB;
  mobilityeNodeB.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityeNodeB.SetPositionAllocator (positionAllocEnb);
  mobilityeNodeB.Install (enbNode);

  //Relay UEs
  MobilityHelper mobilityRelays;
  mobilityRelays.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityRelays.SetPositionAllocator (positionAllocRelays);
  mobilityRelays.Install (relayUeNodes);

  //Remote UE
  MobilityHelper mobilityRemotes;
  mobilityRemotes.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityRemotes.SetPositionAllocator (positionAllocRemotes);
  mobilityRemotes.Install (remoteUeNodes);

  //Install LTE devices to the nodes
  NetDeviceContainer enbDevs = lteHelper->InstallEnbDevice (enbNode);
  NetDeviceContainer relayUeDevs = lteHelper->InstallUeDevice (relayUeNodes);
  NetDeviceContainer remoteUeDevs = lteHelper->InstallUeDevice (remoteUeNodes);
  NetDeviceContainer allUeDevs = NetDeviceContainer (relayUeDevs, remoteUeDevs);

  //Configure Sidelink
  Ptr<LteSlEnbRrc> enbSidelinkConfiguration = CreateObject<LteSlEnbRrc> ();
  enbSidelinkConfiguration->SetSlEnabled (true);

  //Configure communication pool
  LteRrcSap::SlCommTxResourcesSetup pool;

  pool.setup = LteRrcSap::SlCommTxResourcesSetup::UE_SELECTED;
  pool.ueSelected.havePoolToRelease = false;
  pool.ueSelected.havePoolToAdd = true;
  pool.ueSelected.poolToAddModList.nbPools = 1;
  pool.ueSelected.poolToAddModList.pools[0].poolIdentity = 1;

  LteSlResourcePoolFactory pfactory;
  //Control
  pfactory.SetControlPeriod ("sf40");
  pfactory.SetControlBitmap (0x00000000FF); //8 subframes for PSCCH
  pfactory.SetControlOffset (0);
  pfactory.SetControlPrbNum (22);
  pfactory.SetControlPrbStart (0);
  pfactory.SetControlPrbEnd (49);
  //Data
  pfactory.SetDataBitmap (0xFFFFFFFFFF);
  pfactory.SetDataOffset (8); //After 8 subframes of PSCCH
  pfactory.SetDataPrbNum (25);
  pfactory.SetDataPrbStart (0);
  pfactory.SetDataPrbEnd (49);

  pool.ueSelected.poolToAddModList.pools[0].pool =  pfactory.CreatePool ();

  //Add the pool as a default pool
  enbSidelinkConfiguration->SetDefaultPool (pool);

  //Configure discovery pool
  enbSidelinkConfiguration->SetDiscEnabled (true);

  LteRrcSap::SlDiscTxResourcesSetup discPool;
  discPool.setup =  LteRrcSap::SlDiscTxResourcesSetup::UE_SELECTED;
  discPool.ueSelected.havePoolToRelease = false;
  discPool.ueSelected.havePoolToAdd = true;
  discPool.ueSelected.poolToAddModList.nbPools = 1;
  discPool.ueSelected.poolToAddModList.pools[0].poolIdentity = 1;

  LteSlDiscResourcePoolFactory pDiscFactory;
  pDiscFactory.SetDiscCpLen ("NORMAL");
  pDiscFactory.SetDiscPeriod ("rf32");
  pDiscFactory.SetNumRetx (0);
  pDiscFactory.SetNumRepetition (1);
  pDiscFactory.SetDiscPrbNum (10);
  pDiscFactory.SetDiscPrbStart (10);
  pDiscFactory.SetDiscPrbEnd (40);
  pDiscFactory.SetDiscOffset (0);
  pDiscFactory.SetDiscBitmap (0x11111);
  pDiscFactory.SetDiscTxProbability ("p100");

  discPool.ueSelected.poolToAddModList.pools[0].pool =  pDiscFactory.CreatePool ();

  enbSidelinkConfiguration->AddDiscPool (discPool);

  //-Relay UE (re)selection
  LteRrcSap::Sib19DiscConfigRelay discConfigRelay;
  discConfigRelay.haveRemoteUeConfig = true;
  discConfigRelay.remoteUeConfig.haveReselectionInfoIc = true;
  discConfigRelay.remoteUeConfig.reselectionInfoIc.filterCoefficient = 0;
  discConfigRelay.remoteUeConfig.reselectionInfoIc.minHyst = 0;
  discConfigRelay.remoteUeConfig.reselectionInfoIc.qRxLevMin = -125;
  enbSidelinkConfiguration->SetDiscConfigRelay (discConfigRelay);

  //Install Sidelink configuration for eNBs
  lteHelper->InstallSidelinkConfiguration (enbDevs, enbSidelinkConfiguration);

  //Configure Sidelink Preconfiguration for the UEs
  Ptr<LteSlUeRrc> ueSidelinkConfiguration = CreateObject<LteSlUeRrc> ();
  ueSidelinkConfiguration->SetSlEnabled (true);
  LteRrcSap::SlPreconfiguration preconfiguration;
  ueSidelinkConfiguration->SetSlPreconfiguration (preconfiguration);
  ueSidelinkConfiguration->SetDiscEnabled (true);
  uint8_t nb = 3;
  ueSidelinkConfiguration->SetDiscTxResources (nb);
  ueSidelinkConfiguration->SetDiscInterFreq (enbDevs.Get (0)->GetObject<LteEnbNetDevice> ()->GetUlEarfcn ());
  lteHelper->InstallSidelinkConfiguration (relayUeDevs, ueSidelinkConfiguration);
  lteHelper->InstallSidelinkConfiguration (remoteUeDevs, ueSidelinkConfiguration);

  //Install the IP stack on the UEs and assign IP address
  internet.Install (relayUeNodes);
  internet.Install (remoteUeNodes);

  

  Ipv6InterfaceContainer ueIpIfaceRelays;
  Ipv6InterfaceContainer ueIpIfaceRemotes;
  ueIpIfaceRelays = epcHelper->AssignUeIpv6Address (relayUeDevs);
  ueIpIfaceRemotes = epcHelper->AssignUeIpv6Address (remoteUeDevs);

  //Define and set routing
  Ipv6StaticRoutingHelper Ipv6RoutingHelper;
  for (uint32_t u = 0; u <  allUeNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = allUeNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv6StaticRouting> ueStaticRouting = Ipv6RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv6> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress6 (), 1);
    }

  Ipv6AddressHelper ipv6h;
  ipv6h.SetBase (Ipv6Address ("6001:db80::"), Ipv6Prefix (64));
  Ipv6InterfaceContainer internetIpIfaces = ipv6h.Assign (internetDevices);

  internetIpIfaces.SetForwarding (0, true);
  internetIpIfaces.SetDefaultRouteInAllNodes (0);

  Ipv6StaticRoutingHelper ipv6RoutingHelper;
  Ptr<Ipv6StaticRouting> remoteHostStaticRouting = ipv6RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv6> ());
  remoteHostStaticRouting->AddNetworkRouteTo ("7777:f000::", Ipv6Prefix (60), internetIpIfaces.GetAddress (0, 1), 1, 0);


  //Routing downward traffic
  Ptr<Ipv6StaticRouting> pgwStaticRouting = ipv6RoutingHelper.GetStaticRouting (pgw->GetObject<Ipv6> ());
  pgwStaticRouting->AddNetworkRouteTo ("7777:f00e:0:0::", Ipv6Prefix (60), Ipv6Address ("::"), 1, 0);

  //Attach each UE to the best available eNB
  lteHelper->Attach (allUeDevs);

  AsciiTraceHelper ascii;

  Ptr<FlowMonitor> flowMonitor;
  FlowMonitorHelper flowHelper;
  flowMonitor = flowHelper.InstallAll();

  Ptr<ExponentialRandomVariable> emergencyIntervalSeconds = CreateObject<ExponentialRandomVariable> ();
  emergencyIntervalSeconds->SetAttribute("Mean", DoubleValue(simTime));     // To be defined
  emergencyIntervalSeconds->SetAttribute("Bound", DoubleValue(simTime));

  ///*** Configure applications ***///

  // interface 0 is localhost, 1 is the p2p device
  Ipv6Address remoteHostAddr = internetIpIfaces.GetAddress (1, 1);
  uint16_t echoPortBase = 50000;
  ApplicationContainer serverApps;
  ApplicationContainer clientApps;
  uint16_t otherPort = 1234;

  ApplicationContainer emServerApp;
  ApplicationContainer emClientApp;

  //For each UE, we have a pair (UpdEchoClient, UdpEchoServer)
  //Each UE has an assigned port
  //UdpEchoClient installed in the UE, sending to the remoteHost address in the UE port
  //UdpEchoServer installed in the remoteHost, listening to the UE port

  if (relayTraffic)
    {
      for (uint16_t relUeIdx = 0; relUeIdx < relayUeNodes.GetN (); relUeIdx++)
        {
          uint16_t relUePort = echoPortBase + 100 + relUeIdx;

          //UdpEchoServer in the remoteHost for the Remote UE
          UdpEchoServerHelper echoServerHelper (relUePort);
          ApplicationContainer singleServerApp = echoServerHelper.Install (remoteHost);
          singleServerApp.Start (Seconds (1.0));
          singleServerApp.Stop (Seconds (simTime));


          emServerApp.Start (Seconds (1));
          emServerApp.Stop (Seconds (simTime));

          serverApps.Add (singleServerApp);


          //UdpEchoClient in the Remote UE
          UdpEchoClientHelper echoClientHelper (remoteHostAddr);
          echoClientHelper.SetAttribute ("MaxPackets", UintegerValue (8000000));
          echoClientHelper.SetAttribute ("Interval", TimeValue (Seconds (0.20)));
          echoClientHelper.SetAttribute ("PacketSize", UintegerValue (150 - (12)));

          echoClientHelper.SetAttribute ("RemotePort", UintegerValue (relUePort));

          ApplicationContainer singleClientApp = echoClientHelper.Install (relayUeNodes.Get (relUeIdx));
          singleClientApp.Start (Seconds (3.0) + MilliSeconds (relUeIdx * 50) );
          singleClientApp.Stop (Seconds (simTime));
          clientApps.Add (singleClientApp);


        }
    }

  

  if (remoteTraffic)
    {

      for (uint16_t remUeIdx = 0; remUeIdx < remoteUeNodes.GetN (); remUeIdx++)
        {
          uint16_t remUePort = echoPortBase + remUeIdx;

          //UdpEchoServer in the remoteHost for the Remote UE
          UdpEchoServerHelper echoServerHelper (remUePort);
          ApplicationContainer singleServerApp = echoServerHelper.Install (remoteHost);
          singleServerApp.Start (Seconds (1.0));
          singleServerApp.Stop (Seconds (simTime));

          PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory",Inet6SocketAddress (Ipv6Address::GetAny (), otherPort));
          emServerApp.Add(packetSinkHelper.Install (remoteUeNodes.Get(remUeIdx)));
          serverApps.Add (emServerApp);

          serverApps.Add (singleServerApp);


          //UdpEchoClient in the Remote UE
          UdpEchoClientHelper echoClientHelper (remoteHostAddr);
          echoClientHelper.SetAttribute ("MaxPackets", UintegerValue (8000000));
          echoClientHelper.SetAttribute ("Interval", TimeValue (Seconds (0.2)));
          echoClientHelper.SetAttribute ("PacketSize", UintegerValue (150 - (12)));

          echoClientHelper.SetAttribute ("RemotePort", UintegerValue (remUePort));

          ApplicationContainer singleClientApp = echoClientHelper.Install (remoteUeNodes.Get (remUeIdx));
          singleClientApp.Start (Seconds (3.0) + MilliSeconds (remUeIdx * 50) );
          singleClientApp.Stop (Seconds (simTime));
          clientApps.Add (singleClientApp);

          Ipv6Address address = remoteUeNodes.Get(remUeIdx)->GetObject<Ipv6>()->GetAddress(1,1).GetAddress();
          UdpClientHelper client (address, otherPort);
          client.SetAttribute ("Interval", TimeValue (Seconds(emergencyIntervalSeconds->GetValue())));
          client.SetAttribute ("MaxPackets", UintegerValue(1000000));
          // client.SetAttribute("Priority", UintegerValue (1));
          client.SetAttribute("PacketSize", UintegerValue(40));


          emClientApp = client.Install (remoteHost);

          emClientApp.Start (Seconds (Seconds (3.0) + MilliSeconds (remUeIdx * 50) ) );
        //Stop the application after 10.0 s
          emClientApp.Stop (Seconds (simTime));
          clientApps.Add (emClientApp);


        }
    }

     Ipv6Address ip;
  ///*** End of application configuration ***///
    for (uint32_t u = 0; u < remoteUeDevs.GetN (); ++u)
    {
      ip =  remoteUeNodes.Get(u)->GetObject<Ipv6>()->GetAddress(1,1).GetAddress();

      Ptr<MobilityModel> m1 = remoteUeNodes.Get(u)->GetObject<MobilityModel>();
      std::cout<<"IP Address: "<<ip<<"IMSI: "
      << remoteUeNodes.Get(u)->GetDevice(0)->GetObject <LteUeNetDevice>()->GetImsi() << "POS:"<< m1->GetPosition() <<std::endl;
      // std::cout<<"Power"<< ueNodes.Get(u)-> GetDevice(0)->GetObject<LteUePhy>()-> GetTxPower()<<std::endl;
       // *MobiStream->GetStream () << Simulator::Now ().GetSeconds () << "\t" << allUeNodes.Get(u)->GetDevice(0)->GetObject <LteUeNetDevice>()->GetImsi() << "\t"<<m1->GetPosition()<<"\t" <<ip << std::endl;
    } 
     bool useRelay = true; //Use the UE-to-Network Relay
  ///*** Configure Relaying ***///
  if (useRelay)
    {
      proseHelper->SetIpv6BaseForRelayCommunication ("7777:f00e::", Ipv6Prefix (48));

      Ptr<EpcTft> tft = Create<EpcTft> ();
      EpcTft::PacketFilter dlpf;
      dlpf.localIpv6Address.Set ("7777:f00e::");
      dlpf.localIpv6Prefix = Ipv6Prefix (32);
      tft->Add (dlpf);
      EpsBearer bearer (EpsBearer::NGBR_VIDEO_TCP_DEFAULT);
      lteHelper->ActivateDedicatedEpsBearer (relayUeDevs, bearer, tft);

      for (uint32_t ryDevIdx = 0; ryDevIdx < relayUeDevs.GetN (); ryDevIdx++)
        {
          Simulator::Schedule (Seconds (2.0 + 4 * ryDevIdx), &LteSidelinkHelper::StartRelayService, proseHelper, relayUeDevs.Get (ryDevIdx), 33, LteSlUeRrc::ModelA, LteSlUeRrc::RelayUE);
          Simulator::Schedule (Seconds (6.0 + 4 * ryDevIdx), &LteSidelinkHelper::StopRelayService, proseHelper, relayUeDevs.Get (ryDevIdx), 33);
        }
      for (uint32_t rmDevIdx = 0; rmDevIdx < remoteUeDevs.GetN (); rmDevIdx++)
        {
          Simulator::Schedule ((Seconds (4.0 + rmDevIdx)), &LteSidelinkHelper::StartRelayService, proseHelper, remoteUeDevs.Get (rmDevIdx), 33, LteSlUeRrc::ModelA, LteSlUeRrc::RemoteUE);
        }

         //Connect trace in all UEs for monitoring UE-to-Network Relay signaling messages
      Ptr<OutputStreamWrapper> PC5SignalingPacketTraceStream = ascii.CreateFileStream ("PC5SignalingPacketTrace_ranking.txt");
      *PC5SignalingPacketTraceStream->GetStream () << "time(s)\ttxId\tRxId\tmsgType" << std::endl;
      for (uint32_t ueDevIdx = 0; ueDevIdx < allUeDevs.GetN (); ueDevIdx++)
        {
          Ptr<LteUeRrc> rrc = allUeDevs.Get (ueDevIdx)->GetObject<LteUeNetDevice> ()->GetRrc ();
          PointerValue ptrOne;
          rrc->GetAttribute ("SidelinkConfiguration", ptrOne);
          Ptr<LteSlUeRrc> slUeRrc = ptrOne.Get<LteSlUeRrc> ();
          slUeRrc->TraceConnectWithoutContext ("PC5SignalingPacketTrace",
                                               MakeBoundCallback (&TraceSinkPC5SignalingPacketTrace,
                                                                  PC5SignalingPacketTraceStream));
        }
    }

      for (uint32_t i = 0; i < remoteUeDevs.GetN (); i++)
    {
      std::ostringstream txWithAddresses;
      txWithAddresses << "/NodeList/" << remoteHost->GetId () << "/ApplicationList/0/TxWithAddresses";
      Config::ConnectWithoutContext (txWithAddresses.str (), MakeCallback (DataPacketSinkTxNode));

      std::ostringstream rxWithAddresses;
      rxWithAddresses << "/NodeList/" << remoteHost->GetId () << "/ApplicationList/0/RxWithAddresses";
      Config::ConnectWithoutContext (rxWithAddresses.str (), MakeCallback (DataPacketSinkRxNode));
    }


  NS_LOG_INFO ("Starting simulation...");



  Simulator::Stop (Seconds (simTime));

  Simulator::Run ();

  m_addressList.sort ();
  m_addressList.unique ();

      for (auto v : m_addressList)
        std::cout << v << "\n";


  std::cout<< "Address:"<<m_addressList.size ()<<std::endl;

   std::string outputDir = "./";
    std::string simTag= "ranking_factory";
    flowMonitor->CheckForLostPackets ();
    Ptr<Ipv6FlowClassifier> classifier = DynamicCast<Ipv6FlowClassifier> (flowHelper.GetClassifier6 ());
    // FlowMonitor::FlowStatsContainer stats = flowMonitor->GetFlowStats ();
    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

    double averageFlowThroughput = 0.0;
    double averageFlowDelay = 0.0;
    uint32_t sumLostPackets = 0.0;
    uint32_t sumTxPackets = 0.0;
    uint32_t delayPacketSum = 0.0; 
    double delayDrop = 0.0; 

    std::ofstream outFile;
    std::string filename = outputDir + "/" + simTag;
    outFile.open (filename.c_str (), std::ofstream::out | std::ofstream::app);
    if (!outFile.is_open ())
    {
      std::cout<<"Can't open file " << filename;
      return 1;
    }
    outFile.setf (std::ios_base::fixed);

  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
      
      Ipv6FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
      std::stringstream protoStream;
      protoStream << (uint16_t) t.protocol;
      if (t.protocol == 6)
        {
          protoStream.str ("TCP");
        }
      if (t.protocol == 17)
        {
          protoStream.str ("UDP");
        }
    //   for (uint32_t reasonCode = 0; reasonCode < i->second.packetsDropped.size (); reasonCode++)
    // {
    //   outFile<<"Print in";
    //   outFile << "<packetsDropped reasonCode=" << reasonCode
    //       << " number=" << i->second.packetsDropped[reasonCode] << "\n";

    // }
        outFile << "Flow " << i->first << " (" << t.sourceAddress << ":" << t.sourcePort << " -> " << t.destinationAddress << ":" << t.destinationPort << ") proto " << protoStream.str () << "\n"; 
        outFile << "  Tx Packets: " << i->second.txPackets << "\n";
        outFile << "  Tx Bytes:   " << i->second.txBytes << "\n";
        outFile << "  TxOffered:  " << i->second.txBytes * 8.0 / (simTime - 0.01) / 1000 / 1000  << " Mbps\n";
        outFile << "  Rx Bytes:   " << i->second.rxBytes << "\n";
        outFile << "  Lost Packets: " << i->second.lostPackets<< "\n";
        outFile << "  Packet Forwards: "<< i->second.timesForwarded<<"\n";
        // outFile << " Packets Dropped : " << i-> second.packetsDropped<<"\n";
        // outFile << " Byte Dropped: " << i->second.bytesDropped<<"\n";
        sumTxPackets += i->second.txPackets;
        sumLostPackets+= i->second.lostPackets;
      if (i->second.rxPackets > 0)
        {
          // Measure the duration of the flow from receiver's perspective
          double rxDuration = i->second.timeLastRxPacket.GetSeconds () - i->second.timeFirstTxPacket.GetSeconds ();

          averageFlowThroughput += i->second.rxBytes * 8.0 / rxDuration / 1000 / 1000;
          averageFlowDelay += 1000 * i->second.delaySum.GetSeconds () / i->second.rxPackets;
          delayDrop = i->second.lastDelay.GetSeconds(); 
          std::cout << "Delay per packet"<< delayDrop; 
        if (delayDrop > 10)
        {
          delayPacketSum ++; 

        }



          outFile << "  Throughput: " << i->second.rxBytes * 8.0 / rxDuration / 1000 / 1000  << " Mbps\n";
          outFile << "  Mean delay:  " << 1000 * i->second.delaySum.GetSeconds () / i->second.rxPackets << " ms\n";
          //outFile << "  Mean upt:  " << i->second.uptSum / i->second.rxPackets / 1000/1000 << " Mbps \n";
          outFile << "  Mean jitter:  " << 1000 * i->second.jitterSum.GetSeconds () / i->second.rxPackets  << " ms\n";

//          for (uint32_t reasonCode = 0; reasonCode < i->second.packetsDropped.size (); reasonCode++)
//        {
//          outFile<<"Print in";
//          outFile << "<packetsDropped reasonCode=" << reasonCode
//              << " number=" << i->second.packetsDropped[reasonCode] << "\n";
//
//        }
        // std::cout<<i->second.packetsDropped(0)<<std::endl;
        // for (uint32_t reasonCode = 0; reasonCode < i->second.bytesDropped.size (); reasonCode++)
        // {
        //   std::cout<<"\n"<< "<bytesDropped reasonCode=\"" << reasonCode << "\""
        //       << " bytes=\"" << i->second.bytesDropped[reasonCode] << std::endl;
        // }
        }
      else
        {
          outFile << "  Throughput:  0 Mbps\n";
          outFile << "  Mean delay:  0 ms\n";
          outFile << "  Mean upt:  0  Mbps \n";
          outFile << "  Mean jitter: 0 ms\n";
        }
      outFile << "\n" << "Loss Packet %" <<(((i->second.txPackets-i->second.rxPackets)*1.0)/i->second.txPackets);
      // outFile << "\n" << "Lost Packets:" << sumLostPackets;
      outFile << "  Rx Packets: " << i->second.rxPackets << "\n";

    }


    outFile << "\n" << "Packets Transmitted" << sumTxPackets << "\n";
    outFile << "\n" << "Packets Lost" << sumLostPackets << "\n";
    outFile << "\n" << "Packet Lost due to delay"<< delayPacketSum << "\n";
    
    outFile << "\n\n  Mean flow throughput: " << averageFlowThroughput / stats.size() << "\n";
    outFile << "  Mean flow delay: " << averageFlowDelay / stats.size () << "\n";
    outFile <<"----------------------------------------Next Simulation -------------------------------------------------------------------------------------" << "\n";
  outFile.close ();

// flowMonitor->CheckForLostPackets ();

// Ptr<Ipv6FlowClassifier> classifier = DynamicCast<Ipv6FlowClassifier> (flowHelper.GetClassifier6 ());
// std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

// for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
//   {
//   Ipv6FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);
//   if (iter->second.rxPackets>0)
//     {
//       std::cout<<"Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress<<"\n";
//       std::cout<<"Tx Packets = " << iter->second.txPackets<<"\n";
//       std::cout<<"Rx Packets = " << iter->second.rxPackets<<"\n";
//       std::cout<<"Mean Delay = " << (iter->second.delaySum/iter->second.rxPackets).GetMilliSeconds()<<" msec \n";
//       std::cout<<"Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps\n";
//       std::cout  <<std::endl;
//       }
//   }


  /*GtkConfigStore config;
  config.ConfigureAttributes();*/

  flowMonitor->SerializeToXmlFile("ranking_factory.xml", true, true);

  Simulator::Destroy ();

  std::cout << "Rx packets: " <<m_rxPacketNum << "/" << m_txPacketNum<< std::endl;

  return 0;

}
